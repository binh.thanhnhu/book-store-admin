import classNames from "classnames";
import { ROUTES } from "constants/data";
import { keyRoutes } from "constants/interface";
import React from "react";
import { Link, useLocation } from "react-router-dom";
import { findRouteName } from "utils/routes";
import s from "./PageBar.module.css";

const PageBar: React.FC = () => {
  let location = useLocation();

  const routesArray: keyRoutes[] = findRouteName(location, ROUTES).routesArray;

  const handleClick = (event: any, value: number) => {
    event.preventDefault();
    // let object: any;
    // for (let i = 0; i < routesArray.length; i++) {
    //   object = object ? object[routesArray[i]] : ROUTES[routesArray[i]];
    //   if (value === i + 1) break;
    // }
    // history.push(object.path);
  };

  const renderLink = () => {
    let object: any;
    console.log(routesArray);
    return routesArray.map((o: keyRoutes, i: number) => {
      object = object ? object[o] : ROUTES[o];
      if (object && object.title) {
        return (
          <li
            key={object.title}
            className={classNames({
              [s.active]: i + 1 === routesArray.length,
            })}
          >
            {i + 1 === routesArray.length ? (
              <i className="material-icons">chevron_right</i>
            ) : (
              <i className="material-icons">{object.icon}</i>
            )}
            <Link to="" key={i} onClick={(e: any) => handleClick(e, i + 1)}>
              {object.title}
            </Link>
          </li>
        );
      }
      return null;
    });
  };

  const renderTitle = () => {
    if (location.pathname === "/login" || location.pathname === "/") return "";
    let object: any;
    return routesArray.map((o: any, i: number) => {
      object = object ? object[o] : ROUTES[o as keyRoutes];
      return i + 1 === routesArray.length ? object?.title : null;
    });
  };

  if (location.pathname === "/login" || location.pathname === "/") return null;

  return (
    <div className={classNames(s.root, s.pageContentWhite)}>
      <div>
        <div className="pull-left">
          <div className={s.pageTitle}>{renderTitle()}</div>
        </div>
        <ol className={classNames("breadcrumb pull-right", s.pageBreadcrumb)}>
          {routesArray && renderLink()}
        </ol>
      </div>
    </div>
  );
};

export default PageBar;
