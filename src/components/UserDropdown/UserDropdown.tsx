import { useAppDispatch, useAppSelector } from "app/hooks";
import classNames from "classnames";
import { logout } from "features/auth/authSlice";
import useClickOutside from "hooks/useClickOutside";
import React, { createRef, useState } from "react";
import s from "./UserDropdown.module.css";

const UserDropdown = () => {
  const [show, setShow] = useState<boolean>(false);
  const ref = createRef<HTMLLIElement>();
  const user =
    useAppSelector((state) => state.auth.user) ||
    JSON.parse(localStorage.getItem("user_info") || "{}");
  const dispatch = useAppDispatch();

  const handleClick = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    setShow(!show);
  };

  const handleLogout = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    dispatch(logout());
  };

  useClickOutside(ref, show, () => setShow(false));

  return (
    <li
      ref={ref}
      className={classNames("dropdown", s.root, { [s.show]: show })}
    >
      <a
        href="/#"
        className="dropdown-toggle"
        data-toggle="dropdown"
        data-hover="dropdown"
        data-close-others="true"
        onClick={(e) => handleClick(e)}
      >
        <div className="img-circle">
          {user?.username && user?.username[0].toUpperCase()}
        </div>
        <span className={classNames(s.username, s.usernameHideOnMobile)}>
          {" "}
          {user?.firstName}{" "}
        </span>
        <i className="material-icons">expand_more</i>
      </a>
      <ul
        className={classNames(
          "dropdown-menu animated jello",
          s.dropdownMenuDefault
        )}
      >
        {/* <li>
          <a href="user_profile.html">
            <i className="material-icons">person_outline</i> Thông tin{" "}
          </a>
        </li>
        <li>
          <a href="/#">
            <i className="icon-settings" /> Cài đặt
          </a>
        </li>
        <li>
          <a href="/#">
            <i className="icon-directions" /> Trợ giúp
          </a>
        </li>
        <li className="divider"> </li> */}
        <li>
          <a href="/#" onClick={(e) => handleLogout(e)}>
            <i className="icon-logout" /> Đăng xuất{" "}
          </a>
        </li>
      </ul>
    </li>
  );
};

export default UserDropdown;
