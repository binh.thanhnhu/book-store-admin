import { useAppSelector } from "app/hooks";
import classNames from "classnames";
import { logout } from "features/auth/authSlice";
import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import capitalizeString from "utils/capitalizeString";

import s from "./SidebarUserPanel.module.css";

const SidebarUserPanel = () => {
  const user = useAppSelector((state) => state.auth.user);

  const dispatch = useDispatch();

  const handleLogout = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    dispatch(logout());
  };

  return (
    <li className={s.root}>
      <div className={classNames(s.userPanel, s.darkSidebarColor)}>
        <div className="row">
          <div className={s.sidebarUserpic}>
            <div className="img-responsive">
              {user?.username && user?.username[0].toUpperCase()}
            </div>
          </div>
        </div>
        <div className={s.profileUsertitle}>
          <div className={s.sidebarUserpicName}>
            {user && user.lastName + " " + user.firstName}
          </div>
          <div className={s.profileUsertitleJob}>
            {" "}
            {user?.role && capitalizeString(user.role)}{" "}
          </div>
        </div>
        <div className={s.sidebarUserpicBtn}>
          <Link
            to=""
            className="tooltips"
            href="user_profile.html"
            title="Thông tin cá nhân"
          >
            <i className="material-icons">person_outline</i>
          </Link>
          <Link
            to=""
            className="tooltips"
            href="email_inbox.html"
            title="Email"
          >
            <i className="material-icons">mail_outline</i>
          </Link>
          <Link to="" className="tooltips" href="chat.html" title="Chat">
            <i className="material-icons">chat</i>
          </Link>
          <Link
            to=""
            className="tooltips"
            href="login.html"
            title="Đăng xuất"
            onClick={(e) => handleLogout(e)}
          >
            <i className="material-icons">input</i>
          </Link>
        </div>
      </div>
    </li>
  );
};

export default SidebarUserPanel;
