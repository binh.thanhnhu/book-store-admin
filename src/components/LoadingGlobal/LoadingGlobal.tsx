import React from "react";

import s from "./LoadingGlobal.module.css";

const LoadingGlobal = () => {
  return (
    <div className={s.root}>
      <div className={s.container}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default LoadingGlobal;
