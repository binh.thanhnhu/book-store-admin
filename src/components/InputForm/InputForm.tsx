import classNames from "classnames";
import React, { useState } from "react";

import s from "./InputForm.module.css";

interface Props {
  rootClass?: string;
  tag?: "input" | "textarea" | "select" | "upload";
  label: string;
  name: string;
  type?: string;
  list?: {
    val: string;
    text: string;
  }[];
}

const InputForm: React.FC<Props> = ({
  rootClass = "col-lg-6 p-t-24",
  label,
  name,
  type,
  tag = "input",
  list,
}: Props) => {
  const [isFocus, setFocus] = useState<boolean>(false);

  let html;
  switch (tag) {
    case "input":
      html = (
        <div className="mdl-textfield txt-full-width">
          <input
            className={classNames("mdl-textfield__input", s.input, {
              [s.isFocus]: isFocus,
            })}
            type={type}
            id={name}
            name={name}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
          />
          <label className={classNames(s.label, "mdl-textfield__label")}>
            {label}
          </label>
        </div>
      );
      break;
    case "select":
      html = (
        <div className="mdl-textfield txt-full-width">
          <input
            className={classNames("mdl-textfield__input", s.input, {
              [s.isFocus]: isFocus,
            })}
            type={type}
            id={name}
            name={name}
            defaultValue=""
            readOnly
            tabIndex={-1}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
          />
          <div className="pull-right margin-0">
            <i className="mdl-icon-toggle__label material-icons">
              keyboard_arrow_down
            </i>
          </div>
          <label className={classNames(s.label, "mdl-textfield__label")}>
            {label}
          </label>
          <div
            className={classNames("mdl-menu__container", {
              [s.menuOpen]: isFocus,
            })}
            style={{ left: 0, top: 56, width: 124, height: 112 }}
          >
            <div
              className="mdl-menu__outline mdl-menu--bottom-left"
              style={{ width: 124, height: 112 }}
            />
            <ul
              data-mdl-for="sample2"
              className="mdl-menu mdl-menu--bottom-left mdl-js-menu"
              data-upgraded=",MaterialMenu"
            >
              {list?.map((o) => (
                <li className="mdl-menu__item" value={o.val} tabIndex={-1}>
                  {o.text}
                </li>
              ))}
            </ul>
          </div>
        </div>
      );
      break;
    case "textarea":
      html = (
        <div className="mdl-textfield txt-full-width">
          <textarea
            className={classNames("mdl-textfield__input", s.input, {
              [s.isFocus]: isFocus,
            })}
            id={name}
            name={name}
            rows={4}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
          />
          <label
            className={classNames(
              s.label,
              { [s.hide]: tag === "textarea" && isFocus },
              "mdl-textfield__label"
            )}
          >
            {label}
          </label>
        </div>
      );
      break;
    case "upload":
      html = (
        <>
          <label className={classNames(s.controlLabel, "col-md-3")}>
            Upload Room Photos
          </label>
          <form id="id_dropzone" className={s.dropzone}>
            <div className={s.dzMessage}>
              <div className={s.dropIcon}>
                <i className="material-icons">cloud_upload</i>
              </div>
              <h3>Drop files here or click to upload.</h3>
              <em>
                (This is just a demo. Selected files are <strong>not</strong>
                actually uploaded.)
              </em>
            </div>
          </form>
        </>
      );
      break;
    default:
      <></>;
  }

  return <div className={rootClass}>{html}</div>;
};

export default InputForm;
