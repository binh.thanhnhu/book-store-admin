import React, { createRef, useEffect, useRef, useState } from "react";
import classNames from "classnames";
import s from "./Notification.module.css";
import useClickOutside from "hooks/useClickOutside";
import NotificationList from "./NotificationList";
import {
  clearMessageRequest,
  clearNotificationRequest,
} from "features/global/globalSlice";
import { INotification } from "constants/interface";
import { useAppDispatch } from "app/hooks";

interface Props {
  icon: string;
  bgColor: string;
  title: string;
  show: boolean;
  notifications?: INotification[];
  type: "notification" | "message";
  setShow: (type: string) => void;
}

const Notification: React.FC<Props> = ({
  icon,
  bgColor,
  title,
  show,
  notifications,
  type,
  setShow,
}) => {
  const totalUnseenRef = useRef<number>(
    notifications && notifications.length > 0
      ? notifications?.filter((e) => e.seen === false).length
      : 0
  );
  const [notificationsState, setNotificationsState] = useState(
    notifications?.slice(0, 6)
  );
  const indexRef = useRef<number>(6);
  const [index, setIndex] = useState<number>(indexRef.current);
  const ref = createRef<HTMLLIElement>();
  const dispatch = useAppDispatch();

  const handleClick = (event: React.FormEvent<EventTarget>): void => {
    event.preventDefault();
    setShow(type);
  };

  useEffect(() => {
    totalUnseenRef.current =
      notifications?.filter((e) => e.seen === false).length || 0;
    setNotificationsState(notifications?.slice(0, indexRef.current));
    setIndex(indexRef.current);
  }, [notifications]);

  const handleLoadMore = (): void => {
    indexRef.current += 6;
    indexRef.current =
      notifications && indexRef.current + 6 > notifications?.length
        ? notifications.length
        : indexRef.current + 6;
    setNotificationsState(notifications?.slice(0, indexRef.current));
    setIndex(indexRef.current);
  };

  const handleClear = (): void => {
    if (type === "notification") dispatch(clearNotificationRequest());
    if (type === "message") dispatch(clearMessageRequest());
  };

  const handleClose = (): void => {
    setShow(type);
  };

  useClickOutside(ref, show, () => setShow(type));

  return (
    <li
      ref={ref}
      className={classNames("dropdown", s.dropdownExtended, s.root)}
      id="header_notification_bar"
    >
      <a
        href="/#"
        className={classNames("dropdown-toggle", s.dropdown)}
        data-toggle="dropdown"
        data-hover="dropdown"
        data-close-others="true"
        onClick={(e) => handleClick(e)}
      >
        <i className="material-icons">{icon}</i>
        <span className={classNames("badge", s.badge, s[bgColor])}>
          {totalUnseenRef.current}
        </span>
      </a>
      {show && (
        <NotificationList
          title={title}
          notifications={notificationsState}
          total={totalUnseenRef.current}
          index={index}
          type={type}
          handleClear={handleClear}
          loadMore={handleLoadMore}
          handleClose={handleClose}
        />
      )}
    </li>
  );
};

export default Notification;
