import { Tooltip } from "@material-ui/core";
import { useAppDispatch } from "app/hooks";
import classNames from "classnames";
import { NotificationType } from "constants/data";
import { INotification } from "constants/interface";
import {
  updateMessageRequest,
  updateNotificationRequest,
} from "features/global/globalSlice";
import moment from "moment";
import React, { createRef, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import s from "./NotificationList.module.css";

interface Props {
  title: string;
  notifications?: INotification[];
  total?: number;
  index: number;
  type: "notification" | "message";
  handleClear: () => void;
  loadMore: () => void;
  handleClose: () => void;
}

const NotificationList: React.FC<Props> = ({
  title,
  notifications,
  total,
  index,
  type,
  handleClear,
  loadMore,
  handleClose,
}) => {
  const listRef = createRef<HTMLUListElement>();
  const active = useRef(false);
  const dispatch = useAppDispatch();
  let nofificationMap: any = {};
  NotificationType.forEach((e) => (nofificationMap[e.value] = e));

  useEffect(() => {
    return () => {
      active.current = true;
    };
  }, []);

  useEffect(() => {
    return () => {
      if (active.current) {
        if (type === "notification") dispatch(updateNotificationRequest(index));
        if (type === "message") dispatch(updateMessageRequest(index));
        active.current = false;
      }
    };
  }, [dispatch, index, active, type]);

  useEffect(() => {
    listRef?.current?.addEventListener("scroll", () => {
      if (
        listRef.current &&
        listRef.current.scrollTop + listRef.current.clientHeight >=
          listRef.current.scrollHeight
      ) {
        loadMore();
      }
    });
  }, [listRef, loadMore]);

  const handleNotificationsClear = (
    event: React.FormEvent<EventTarget>
  ): void => {
    event.preventDefault();
    handleClear();
  };

  const handleRedirect = (
    event: React.FormEvent<EventTarget>,
    path: string | undefined
  ): void => {
    if (!path) event.preventDefault();
    handleClose();
  };

  return (
    <ul className={classNames("dropdown-menu animated swing", s.dropdownMenu)}>
      <li className={s.external}>
        <h3>
          <span className="bold">{title}</span>
        </h3>
        <span className="notification-label purple-bgcolor">{`New ${total}`}</span>
      </li>
      <li>
        <div
          className="slimScrollDiv"
          style={{
            position: "relative",
            overflow: "hidden",
            width: "auto",
          }}
        >
          <ul ref={listRef} className={s.dropdownMenuList}>
            {notifications?.map((notification, index) => (
              <li key={index}>
                <Link
                  to={notification.path || ""}
                  className={classNames({ [s.seen]: notification.seen })}
                  onClick={(e) => handleRedirect(e, notification.path)}
                >
                  <span className={s.time}>
                    {moment(notification.time).fromNow(true)}
                  </span>
                  <span className={s.details}>
                    <span
                      className={classNames(
                        s.notificationIcon,
                        `notification-icon circle ${
                          nofificationMap[notification.option].backgroundColor
                        }`
                      )}
                    >
                      <i className="material-icons">
                        {nofificationMap[notification.option].icon}
                      </i>
                    </span>{" "}
                    <Tooltip
                      title={
                        <span
                          style={{
                            fontSize: 14,
                          }}
                        >
                          {notification.message}
                        </span>
                      }
                      placement="bottom"
                      arrow
                    >
                      <span className={s.message}>{notification.message}</span>
                    </Tooltip>
                  </span>
                </Link>
              </li>
            ))}
          </ul>
          <div
            className="slimScrollBar"
            style={{
              background: "rgb(158, 165, 171)",
              width: 5,
              position: "absolute",
              top: 0,
              opacity: "0.4",
              display: "block",
              borderRadius: 7,
              zIndex: 99,
              right: 1,
            }}
          />
          <div
            className="slimScrollRail"
            style={{
              width: 5,
              height: "100%",
              position: "absolute",
              top: 0,
              display: "none",
              borderRadius: 7,
              background: "rgb(51, 51, 51)",
              opacity: "0.2",
              zIndex: 90,
              right: 1,
            }}
          />
        </div>
        <div className={s.dropdownMenuFooter}>
          <a href="/#" onClick={(e) => handleNotificationsClear(e)}>
            {" "}
            Clear notifications{" "}
          </a>
        </div>
      </li>
    </ul>
  );
};

export default NotificationList;
