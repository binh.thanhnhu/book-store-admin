import React, { useEffect, useState } from "react";

import SidebarUserPanel from "components/SidebarUserPanel/SidebarUserPanel";
import NavItem from "components/NavItem/NavItem";

import s from "./Sidebar.module.css";
import classNames from "classnames";
import { ROUTES } from "constants/data";
import { useLocation } from "react-router-dom";
import { findRouteName } from "utils/routes";
import { useAppSelector } from "app/hooks";
import { keyRoutes } from "constants/interface";

const Sidebar: React.FC = () => {
  const [itemLv1Select, setItemLv1Select] = useState<{
    [key: number]: boolean;
  }>({ 0: true });
  const [itemLv1Active, setItemLv1Active] = useState<{
    [key: number]: boolean;
  }>({ 0: true });
  const [itemLv2Active, setItemLv2Active] = useState<{
    [key: number]: boolean;
  }>({ 0: true });
  const user = useAppSelector((state) => state.auth.user);
  const location = useLocation();

  const handleSetItemLv1Select = (index: number): void => {
    setItemLv1Select({ [index]: !itemLv1Select[index] });
  };

  useEffect(() => {
    if (location.pathname === "/login" || location.pathname === "/") return;
    const routesArray: keyRoutes[] = findRouteName(
      location,
      ROUTES
    ).routesArray;
    let object: any;
    const arr = routesArray.map((o: any, i: number) => {
      object = object ? object[o] : ROUTES[o as keyRoutes];
      return object?.id;
    });
    setItemLv1Select({ [arr[0]]: true });
    setItemLv1Active({ [arr[0]]: true });
    setItemLv2Active({ [arr[2]]: true });
  }, [location]);

  if (location.pathname === "/login" || location.pathname === "/") return null;

  return (
    <div className={s.root}>
      <div
        className={classNames(
          "navbar-collapse collapse",
          s.sideMenuContainer,
          s.fixedMenu,
          s.darkSidebarColor
        )}
      >
        <div id="remove-scroll">
          <ul
            className={s.sidebarMenu}
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed={200}
          >
            <li className={classNames("hide", s.sidebarTogglerWrapper)}>
              <div className={s.sidebarToggler}>
                <span />
              </div>
            </li>
            <SidebarUserPanel />
            {Object.keys(ROUTES).map((itemName) => (
              <NavItem
                key={ROUTES[itemName as keyRoutes].id}
                select={itemLv1Select[ROUTES[itemName as keyRoutes].id]}
                activeLv1={itemLv1Active}
                activeLv2={itemLv2Active}
                id={ROUTES[itemName as keyRoutes].id}
                icon={ROUTES[itemName as keyRoutes].icon}
                content={ROUTES[itemName as keyRoutes].title}
                child={ROUTES[itemName as keyRoutes].data}
                setItemLv1Select={handleSetItemLv1Select}
                role={user?.role}
              />
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
