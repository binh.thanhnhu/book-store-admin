import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextFieldMui, { TextFieldProps } from "@material-ui/core/TextField";
import { FieldProps } from "formik";
import React from "react";

interface Props {}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      "& .MuiFormLabel-root": {
        color: "var(--main-color)",
      },
      "& .MuiInputBase-input": {
        color: "var(--third-color)",
      },
    },
  })
);

const TextField: React.FC<Props & FieldProps & TextFieldProps> = ({
  label,
  field,
  form,
  select,
  children,
  multiline,
  rowsMax,
  type,
  disabled,
}: Props & FieldProps & TextFieldProps) => {
  const classes = useStyles();
  const { name, value, onChange } = field;
  const { errors, touched } = form;

  return (
    <TextFieldMui
      error={!!errors[name] && !!touched[name]}
      disabled={disabled}
      type={type}
      label={label}
      name={name}
      multiline={multiline}
      rowsMax={rowsMax}
      className={classes.root}
      select={select}
      value={value || ""}
      onChange={onChange}
      InputLabelProps={
        type === "date"
          ? {
              shrink: type === "date",
            }
          : undefined
      }
      helperText={touched[name] && errors[name]}
    >
      {select && children}
    </TextFieldMui>
  );
};

export default TextField;
