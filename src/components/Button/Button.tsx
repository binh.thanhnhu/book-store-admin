import React from "react";
import clsx from "clsx";
import ButtonMui, { ButtonProps } from "@material-ui/core/Button";
import { withStyles, WithStyles } from "@material-ui/core/styles";

interface Props extends WithStyles<typeof styles> {
  children?: React.ReactNode;
  className?: string;
  variant: "text" | "outlined" | "contained" | undefined;
}

const styles = {
  root: {},
};

const Button = (props: Props & ButtonProps) => {
  const { classes, children, className, ...other } = props;

  return (
    <ButtonMui className={clsx(classes.root, className)} {...other}>
      {children}
    </ButtonMui>
  );
};

export default withStyles(styles)(Button);
