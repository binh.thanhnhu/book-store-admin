import { Button } from "@material-ui/core";
import classNames from "classnames";
import Card from "components/Card/Card";
import { IOrder } from "constants/interface";
import moment from "moment";
import formatPrice from "utils/formatPrice";
import s from "./Information.module.css";

interface Props {
  item: any;
  itemMap: any;
  order?: boolean;
  status?: "pending" | "verified" | "cancelled";
  handleStatusChange?: (status: IOrder["status"]) => void;
}

const Information = ({
  status,
  item,
  itemMap,
  order,
  handleStatusChange,
}: Props) => {
  const renderBodyOrder = () => {
    const temp = item.cart?.info.map((element: any) => {
      const tempv2: any = {};
      itemMap.cart?.child[0].data.forEach((elementv2: any) => {
        tempv2[elementv2.key] =
          elementv2.key === "price"
            ? formatPrice(element[elementv2.key]) + " đ"
            : element[elementv2.key];
      });
      return tempv2;
    });

    return temp?.map((tr: any, i: number) => (
      <tr key={i}>
        {itemMap.cart?.child[0].data.map((td: any, j: number) => (
          <td className={s.tdOrder} key={j}>
            {tr[td.key]}
          </td>
        ))}
      </tr>
    ));
  };

  const renderActions = () => {
    switch (status) {
      case "pending":
        return (
          <>
            <Button
              type="submit"
              variant="contained"
              className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("verified")}
            >
              Duyệt
            </Button>
            <Button
              variant="contained"
              className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("cancelled")}
            >
              Hủy bỏ
            </Button>
          </>
        );
      case "verified":
        return (
          <>
            <Button
              type="submit"
              variant="contained"
              className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("pending")}
            >
              Chờ
            </Button>
            <Button
              variant="contained"
              className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("cancelled")}
            >
              Hủy bỏ
            </Button>
          </>
        );
      case "cancelled":
        return (
          <Button
            type="submit"
            variant="contained"
            className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
            onClick={changeStatusOrder("pending")}
          >
            Chờ
          </Button>
        );
      default:
        return (
          <>
            <Button
              type="submit"
              variant="contained"
              className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("verified")}
            >
              Duyệt
            </Button>
            <Button
              variant="contained"
              className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
              onClick={changeStatusOrder("cancelled")}
            >
              Hủy bỏ
            </Button>
          </>
        );
    }
  };

  const changeStatusOrder = (status: IOrder["status"]) => (): void => {
    handleStatusChange && handleStatusChange(status);
  };

  return (
    <>
      <Card header="Thông tin">
        <table className={s.root}>
          <tbody>
            {item &&
              Object.keys(item).map(
                (key: any) =>
                  ((order && key !== "cart") || !order) && (
                    <tr
                      key={key}
                      className={classNames({
                        [s.trImage]: key === "imageFromUrl",
                      })}
                    >
                      <th>{itemMap[key].label}</th>
                      <td>
                        {key === "imageFromUrl" ? (
                          <div className={s.wrapImage}>
                            <div>
                              <img
                                src={
                                  item[key] && item[key][0] && item[key][0].url
                                }
                                alt="imageFromUrl"
                                className={s.image}
                              />
                            </div>
                          </div>
                        ) : (
                          <p>{item[key]}</p>
                        )}
                      </td>
                    </tr>
                  )
              )}
          </tbody>
        </table>
      </Card>
      {order && (
        <Card header="Hóa đơn">
          <table className={s.root}>
            <thead>
              <tr className={s.trOder}>
                {itemMap.cart?.child[0].data.map((th: any, i: number) => (
                  <th key={i}>{th.label}</th>
                ))}
              </tr>
            </thead>
            <tbody>{renderBodyOrder()}</tbody>
          </table>
          <table className={classNames(s.root, s.after)}>
            <thead>
              <tr>
                <th>{itemMap.cart.child[1].label}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{formatPrice(item.cart?.totalPrice || 0) + " đ"}</td>
              </tr>
            </tbody>
          </table>
          <table className={classNames(s.root, s.after)}>
            <thead>
              <tr>
                <th>{itemMap.cart.child[2].label}</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {moment(item.cart?.time).format("MM-DD-YYYY") +
                    " (MM-DD-YYYY)"}
                </td>
              </tr>
            </tbody>
          </table>
          <div className="col-lg-12 p-t-24 text-center m-t-10">
            {renderActions()}
          </div>
        </Card>
      )}
    </>
  );
};

export default Information;
