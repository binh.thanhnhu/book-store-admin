import classNames from "classnames";
import React from "react";

import s from "./Alert.module.css";

interface Props {
  type: "success" | "warning" | "error" | "info";
  children: string;
}

const Alert: React.FC<Props> = ({ type, children }: Props) => {
  return <div className={classNames("alert", s[type])}>{children}</div>;
};

export default Alert;
