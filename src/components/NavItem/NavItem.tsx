import classNames from "classnames";
import useCollapse from "hooks/useCollapse";
import React, { createRef } from "react";
import { Link } from "react-router-dom";

import s from "./NavItem.module.css";

interface Props {
  id: number;
  icon: string;
  content: string;
  child: any;
  select: boolean;
  activeLv1: { [key: number]: boolean };
  activeLv2: { [key: number]: boolean };
  role?: "admin" | "staff";
  setItemLv1Select: (index: number) => void;
}

const NavItem: React.FC<Props> = ({
  id,
  icon,
  content,
  child,
  select,
  activeLv1,
  activeLv2,
  role,
  setItemLv1Select,
}: Props) => {
  const ulRef = createRef<HTMLUListElement>();
  const { height } = useCollapse(ulRef, 0, select, "sidebar");

  const handleItemLv1Select = (
    event: React.FormEvent<EventTarget>,
    index: number
  ): void => {
    event.preventDefault();
    setItemLv1Select(index);
  };

  return (
    <li className={classNames("nav-item", s.root, { [s.active]: select })}>
      <a
        href="/#"
        className={classNames("nav-link nav-toggle", s.navLink)}
        onClick={(e) => handleItemLv1Select(e, id)}
      >
        <i className="material-icons">{icon}</i>
        <span className="title">{content}</span>
        {select ? (
          <i className="material-icons">expand_more</i>
        ) : (
          <i className="material-icons">chevron_right</i>
        )}
      </a>
      <ul ref={ulRef} style={{ height }} className={classNames(s.subMenu)}>
        {Object.keys(child).map((itemName: string, index: number) => (
          <li
            key={itemName}
            className={classNames(
              "nav-item",
              { hide: child[itemName].role === "admin" && role !== "admin" },
              {
                [s.navItemActive]: activeLv1[id] && activeLv2[index] === true,
              }
            )}
          >
            <Link
              to={child[itemName].path}
              onClick={
                child[itemName].preventClick
                  ? (e) => e.preventDefault()
                  : () => {}
              }
              className={classNames("nav-link", {
                [s.disabled]: child[itemName].preventClick,
              })}
            >
              <span className="title">{child[itemName].title}</span>
            </Link>
          </li>
        ))}
      </ul>
    </li>
  );
};

export default NavItem;
