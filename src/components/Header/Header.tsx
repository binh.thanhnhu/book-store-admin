import Searchbox from "components/Searchbox/Searchbox";
import TopMenu from "components/TopMenu/TopMenu";
import React from "react";

import s from "./Header.module.css";
import classNames from "classnames";
import { useLocation } from "react-router-dom";

interface Props {
  handleMobileSidebarOpen: () => void;
}

const Header: React.FC<Props> = ({ handleMobileSidebarOpen }) => {
  const location = useLocation();
  if (location.pathname === "/login" || location.pathname === "/") return null;

  const handleMobileSidebar = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    handleMobileSidebarOpen();
  };

  return (
    <div className={classNames(s.root, s.pageHeader, s.headerWhite)}>
      <div className={s.pageInner}>
        <div className={classNames(s.pageLogo, s.pageLogoDark)}>
          <a href="index.html">
            <img alt="" src="assets/img/logo.png" />
            <span className={s.logoDefault}>Pustaka</span>{" "}
          </a>
        </div>
        <ul className={classNames("nav", "navbar-nav", s.navbarLeft, s.in)}>
          <li>
            <a
              href="/#"
              className={s.menuToggler}
              onClick={(e) => handleMobileSidebar(e)}
            >
              <i className="material-icons">menu</i>
            </a>
          </li>
        </ul>
        <Searchbox />
        <a
          href="/#"
          className={classNames(s.menuToggler, s.responsiveToggler)}
          data-toggle="collapse"
          data-target=".navbar-collapse"
        >
          <span />
        </a>
        <TopMenu />
      </div>
    </div>
  );
};

export default Header;
