import { ArrayElement, IProduct } from "constants/interface";
import React, { useEffect, useReducer, useRef, useState } from "react";
import Button from "@material-ui/core/Button";

import s from "./FileUpload.module.css";
import classNames from "classnames";

interface Props {
  imageFromUrl?: IProduct["imageFromUrl"] | undefined;
  loading?: boolean;
  handleImagesSource: (
    image: IProduct["image"],
    imagesDelete: string[],
    imagesInfo: { quantity: number; totalSize: number }
  ) => void;
}

const FileUpload = ({ handleImagesSource, imageFromUrl, loading }: Props) => {
  const [fileInputState, setFileInputState] = useState<string>("");
  const [imagesDelete, setImagesDelete] = useState<string[]>([]);
  const [imageFromUrlState, setImageFromUrlState] = useState<
    IProduct["imageFromUrl"] | undefined
  >([]);
  //eslint-disable-next-line
  const [_, forceUpdate] = useReducer((x) => x + 1, 0);
  let filesInfoArray = useRef<File[]>([]);
  let imagesSource = useRef<IProduct["image"]>([]);
  let imagesInfo = useRef<{ quantity: number; totalSize: number }>({
    quantity: 0,
    totalSize: 0,
  });
  let firstTime = useRef<boolean>(true);

  useEffect(() => {
    setImageFromUrlState(imageFromUrl);
    imagesInfo.current.quantity = imageFromUrl?.length || 0;
    if (firstTime && imageFromUrl) {
      imageFromUrl.forEach((e) => (imagesInfo.current.totalSize += e.fileSize));
      firstTime.current = false;
    }
  }, [imageFromUrl]);

  useEffect(() => {
    if (!loading) {
      imagesSource.current = [];
      handleImagesSource(
        imagesSource.current,
        imagesDelete,
        imagesInfo.current
      );
      setImagesDelete([]);
      if (firstTime && imageFromUrl) {
        imagesInfo.current.totalSize = 0;
        imageFromUrl.forEach(
          (e) => (imagesInfo.current.totalSize += e.fileSize)
        );
        firstTime.current = false;
      }
      setFileInputState("");
    }
    // eslint-disable-next-line
  }, [loading, imageFromUrl]);

  const handleFileInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const files = event.target.files;
    previewFile(files);
    setFileInputState(event.target.value);
    forceUpdate();
    handleImagesSource(imagesSource.current, imagesDelete, imagesInfo.current);
  };

  const previewFile = (files: any) => {
    for (let i = 0; i < files.length; i++) {
      filesInfoArray.current.push(files[i]);
      const reader = new FileReader();
      reader.onloadend = () => {
        imagesSource.current.push({
          source: reader.result,
          info: { fileSize: files[i].size, fileType: files[i].type },
          id: files[i].lastModified,
        });
        forceUpdate();
      };
      reader.readAsDataURL(files[i]);
      imagesInfo.current.quantity++;
      imagesInfo.current.totalSize += files[i].size;
    }
  };

  const handleImageFromUrlDelete =
    (image: ArrayElement<IProduct["imageFromUrl"]>) => (): void => {
      setImagesDelete([...imagesDelete, image.public_id]);
      imagesInfo.current.quantity--;
      const temp = imageFromUrlState?.filter(
        (e) => e.public_id !== image.public_id
      );
      setImageFromUrlState(temp);
      imagesInfo.current.totalSize -= image.fileSize;
      forceUpdate();
      handleImagesSource(
        imagesSource.current,
        [...imagesDelete, image.public_id],
        imagesInfo.current
      );
    };

  const handleImageDelete =
    (id: string, fileSize: number | undefined) => (): void => {
      imagesInfo.current.quantity--;
      imagesSource.current = imagesSource.current.filter((e) => e.id !== id);
      if (fileSize) imagesInfo.current.totalSize -= fileSize;
      forceUpdate();
      handleImagesSource(
        imagesSource.current,
        imagesDelete,
        imagesInfo.current
      );
    };

  return (
    <div className={s.root}>
      <div className="row">
        <div className="col-12 col-sm-3">
          <p className="title">Upload ảnh</p>
          <Button variant="contained" component="label" color="primary">
            Tải lên
            <input
              id="fileInput"
              type="file"
              name="image"
              multiple
              onChange={handleFileInputChange}
              value={fileInputState}
              hidden
            />
          </Button>
        </div>
        <div className={classNames(s.imageFromUrl, "col-12 col-sm-9")}>
          {imageFromUrlState &&
            imageFromUrlState.map((image, i) => (
              <span key={i}>
                <div>
                  <div className={s.darken}></div>
                  <div
                    className={s.delete}
                    onClick={handleImageFromUrlDelete(image)}
                  >
                    <span className="material-icons">delete_outline</span>
                  </div>
                  <img src={image.url} alt="chosen" />
                </div>
              </span>
            ))}
          {imagesSource.current.map(
            (p: ArrayElement<IProduct["image"]>, i: number) => (
              <span key={i}>
                <div>
                  <div className={s.darken}></div>
                  <div
                    className={s.delete}
                    onClick={handleImageDelete(p.id, p.info?.fileSize)}
                  >
                    <span className="material-icons">delete_outline</span>
                  </div>
                  <img src={p.source as string} alt="chosen" width={100} />
                </div>
              </span>
            )
          )}
        </div>
      </div>
    </div>
  );
};

export default FileUpload;
