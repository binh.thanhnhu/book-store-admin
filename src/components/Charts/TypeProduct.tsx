import { ApexOptions } from "apexcharts";
import { useAppSelector } from "app/hooks";
import Card from "components/Card/Card";
import React, { useMemo } from "react";
import ReactApexChart from "react-apexcharts";

const TypeProductOption: ApexOptions = {
  chart: {
    animations: {
      enabled: true,
      easing: "easeinout",
      speed: 800,
      animateGradually: {
        enabled: true,
        delay: 150,
      },
      dynamicAnimation: {
        enabled: true,
        speed: 350,
      },
    },
  },
  colors: ["#0a58ca", "#198754", "#e67d21", "#8e44ad"],
  labels: [
    "Sách Việt Nam",
    "Sách Nước Ngoài",
    "Văn Phòng Phẩm",
    "Quà Lưu Niệm",
  ],
  legend: {
    position: "bottom",
    itemMargin: {
      vertical: 8,
    },
    labels: {
      colors: ["var(--third-color)"],
    },
  },
  dataLabels: {
    enabled: true,
    dropShadow: { enabled: false },
  },
  plotOptions: {
    pie: {
      offsetY: 30,
      donut: {
        labels: {
          value: {
            color: "var(--third-color)",
          },
          show: true,
          total: {
            show: true,
            label: "Tổng",
            fontSize: "16px",
            fontWeight: 600,
            color: "var(--third-color)",
            formatter: function (w) {
              return w.globals.seriesTotals.reduce((a: number, b: number) => {
                return a + b;
              }, 0);
            },
          },
        },
      },
    },
  },
};

const TypeProduct: React.FC = () => {
  const statistics = useAppSelector((state) => state.statistics.statistics);

  const TypeProductData = useMemo(
    () => [
      statistics.totalVietnameseBooks,
      statistics.totalForeignBooks,
      statistics.totalStationeries,
      statistics.totalSouvenirs,
    ],
    [statistics]
  );

  return (
    <Card minHeight={490} shadow header="Loại sản phẩm">
      <ReactApexChart
        type="donut"
        series={TypeProductData}
        options={TypeProductOption}
        height={400}
      />
    </Card>
  );
};

export default TypeProduct;
