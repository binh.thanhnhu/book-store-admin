import { ApexOptions } from "apexcharts";
import { useAppDispatch, useAppSelector } from "app/hooks";
import Card from "components/Card/Card";
import {
  getOrdersOfWeekRequest,
  getSalesOfWeekRequest,
} from "features/statistics/statisticsSlice";
import moment from "moment";
import React, { useEffect, useMemo } from "react";
import ReactApexChart from "react-apexcharts";

const SaleAndOrderOptions: ApexOptions = {
  chart: {
    animations: {
      enabled: true,
      easing: "easeinout",
      speed: 800,
      animateGradually: {
        enabled: true,
        delay: 150,
      },
      dynamicAnimation: {
        enabled: true,
        speed: 350,
      },
    },
  },
  stroke: {
    width: 3,
    curve: "smooth",
    lineCap: "round",
  },
  colors: ["#0a58ca", "#198754"],
  fill: {
    opacity: 1,
    gradient: {
      type: "vertical",
      shadeIntensity: 0,
      opacityFrom: 0.4,
      opacityTo: 0,
      stops: [0, 100],
    },
  },
  legend: {
    labels: {
      colors: ["var(--third-color)"],
    },
  },
  xaxis: {
    categories: [
      moment().subtract(6, "days").format("ddd"),
      moment().subtract(5, "days").format("ddd"),
      moment().subtract(4, "days").format("ddd"),
      moment().subtract(3, "days").format("ddd"),
      moment().subtract(2, "days").format("ddd"),
      moment().subtract(1, "days").format("ddd"),
      moment().format("ddd"),
    ],
  },
};

const SaleAndOrder: React.FC = () => {
  const salesOfWeek = useAppSelector((state) => state.statistics.salesOfWeek);
  const ordersOfWeek = useAppSelector((state) => state.statistics.ordersOfWeek);
  const dispatch = useAppDispatch();

  const SaleAndOrderData = useMemo(
    () => [
      {
        name: "Số lượng bán ra",
        type: "line",
        data: salesOfWeek,
      },
      {
        name: "Số lượng đặt hàng",
        type: "column",
        data: ordersOfWeek,
      },
    ],
    [salesOfWeek, ordersOfWeek]
  );

  useEffect(() => {
    dispatch(getSalesOfWeekRequest());
    dispatch(getOrdersOfWeekRequest());
  }, [dispatch]);

  return (
    <Card minHeight={490} shadow header="Số lượng đặt hàng / Số lượng bán ra">
      <ReactApexChart
        series={SaleAndOrderData}
        options={SaleAndOrderOptions}
        height={400}
      />
    </Card>
  );
};

export default SaleAndOrder;
