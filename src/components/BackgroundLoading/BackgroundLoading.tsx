import React from "react";

import s from "./BackgroundLoading.module.css";

const BackgroundLoading = () => {
  return (
    <div className={s.root}>
      <div className={s.loader}></div>;
    </div>
  );
};

export default BackgroundLoading;
