import { useAppDispatch, useAppSelector } from "app/hooks";
import classNames from "classnames";
import Notification from "components/Notification/Notification";
import UserDropdown from "components/UserDropdown/UserDropdown";
import {
  changeDarkMode,
  getAllMessagesRequest,
} from "features/global/globalSlice";
import React, { useEffect, useState } from "react";
import Switch from "@material-ui/core/Switch";
import s from "./TopMenu.module.css";

const TopMenu = () => {
  const [showNotification, setShowNotification] = useState<boolean>(false);
  const [showMessage, setShowMessage] = useState<boolean>(false);
  const isDarkMode = useAppSelector((state) => state.global.darkmode);
  const notifications = useAppSelector((state) => state.global.notifications);
  const messages = useAppSelector((state) => state.global.messages);

  const handleSetShow = (type: string): void => {
    if (type === "notification") setShowNotification(!showNotification);
    if (type === "message") setShowMessage(!showMessage);
  };

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getAllMessagesRequest());
    const timmer = setInterval(() => {
      dispatch(getAllMessagesRequest());
    }, 1 * 60 * 1000);
    return () => {
      clearInterval(timmer);
    };
  }, [dispatch]);

  const handleModeChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    dispatch(changeDarkMode(event.target.checked));
  };

  return (
    <div className={s.root}>
      <ul className={classNames("nav navbar-nav pull-right", s.nav)}>
        <Notification
          icon="notifications_none"
          bgColor="headerBadgeColor1"
          title="Notifications"
          show={showNotification}
          setShow={handleSetShow}
          notifications={notifications}
          type="notification"
        />
        <Notification
          icon="mail_outline"
          bgColor="headerBadgeColor2"
          title="Messages"
          show={showMessage}
          setShow={handleSetShow}
          type="message"
          notifications={messages}
        />
        <UserDropdown />
        <li className={classNames("dropdown", s.dropdown)}>
          <Switch
            checked={isDarkMode}
            onChange={handleModeChange}
            name="darkmode"
            inputProps={{ "aria-label": "primary checkbox" }}
          />
          <span>Dark Mode</span>
        </li>
      </ul>
    </div>
  );
};

export default TopMenu;
