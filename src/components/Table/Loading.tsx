import classNames from "classnames";
import React from "react";

import s from "./Loading.module.css";

const Loading = () => {
  return (
    <div className={s.root}>
      <div className={s.container}>
        <div className={classNames(s.dot, s.dot1)}></div>
        <div className={classNames(s.dot, s.dot2)}></div>
        <div className={classNames(s.dot, s.dot3)}></div>
      </div>
    </div>
  );
};

export default Loading;
