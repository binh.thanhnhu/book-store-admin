import classNames from "classnames";
import calcItems from "utils/calcItems";
import useCollapse from "hooks/useCollapse";
import useSetHeightTable from "hooks/useSetHeightTable";
import React, { createRef, Fragment, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import addParams from "utils/addParams";
import Body from "./Body";

import s from "./Table.module.css";
import { LinearProgress } from "@material-ui/core";
import { keyRoutes } from "constants/interface";
import ImageShow from "components/ImageShow/ImageShow";
import useClickOutside from "hooks/useClickOutside";

interface Props {
  title: string;
  showTitle?: boolean;
  maxHeightProps?: number;
  table_th: { key: string; label: string }[];
  table_tr: any;
  minWidth?: number;
  perPage: string;
  page: string;
  arr: string[];
  totalItems: number;
  sortAsc: { [key: string]: boolean };
  keywordAllField: string;
  loading: boolean;
  collapsible?: boolean;
  variant?: keyRoutes;
  error?: string;
  handlePerPage?: (perPage: string) => void;
  handleSort: (key: string) => void;
  handleOnChange: (value: string) => void;
  handleDelete: (id: string) => void;
  handleTableReset: () => void;
}

const Table: React.FC<Props> = ({
  title,
  showTitle,
  maxHeightProps,
  table_th,
  table_tr,
  minWidth,
  perPage = "12",
  page = "1",
  arr,
  totalItems,
  sortAsc,
  keywordAllField,
  loading,
  collapsible,
  variant,
  error,
  handlePerPage,
  handleSort,
  handleOnChange,
  handleDelete,
  handleTableReset,
}: Props) => {
  const imageRef = createRef<HTMLDivElement>();
  const divRef = createRef<HTMLDivElement>();
  const tableRef = createRef<HTMLDivElement>();
  const location = useLocation();

  const [imageShow, setImageShow] = useState<boolean>(false);
  const [urlImage, setUrlImage] = useState<string>("");

  const { collapse, setCollapse, height } = useCollapse(divRef, 0);
  const { maxHeight } = useSetHeightTable(tableRef, 75);
  const { min, max } = calcItems(totalItems, perPage, page, "asc");

  useClickOutside(imageRef, imageShow, () => setImageShow(false));

  const _handlePerPage = (
    event: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    handlePerPage && handlePerPage(event.target.value);
  };

  const handleImageShow = (url: string) => {
    setUrlImage(url);
    setImageShow(true);
  };

  return (
    <>
      {imageShow && (
        <div className={s.imageContainer}>
          <div ref={imageRef} className={s.imageWrap}>
            <ImageShow urlImage={urlImage} />
          </div>
        </div>
      )}

      <div className="row">
        <div className="col-md-12 col-sm-12">
          <div
            ref={divRef}
            style={collapsible ? { height } : {}}
            className={classNames("card", s.cardBox)}
          >
            <div>
              <LinearProgress
                variant={loading ? "indeterminate" : "determinate"}
                value={100}
              />
              <div className={s.cardHead}>
                <header>{(collapse || showTitle) && title}</header>
                <div className={s.tools}>
                  <a
                    href="/#"
                    onClick={(e) => {
                      e.preventDefault();
                      handleTableReset();
                    }}
                  >
                    <i className="material-icons">refresh</i>
                  </a>
                  {collapsible && (
                    <a
                      href="/#"
                      onClick={(e) => {
                        e.preventDefault();
                        setCollapse(!collapse);
                      }}
                    >
                      <i className="material-icons">expand_more</i>
                    </a>
                  )}
                </div>
              </div>
              <div className={classNames("card-body", s.cardBody)}>
                <div className={s.tableWrap}>
                  <div className="row">
                    <div className="col-sm-12 col-md-6">
                      <div className={s.dataTablesLength}>
                        <label>
                          Show{" "}
                          <select
                            name="length_category"
                            onChange={(e) => _handlePerPage(e)}
                            value={perPage}
                            className="form-control form-control-sm"
                          >
                            <option value={12}>12</option>
                            <option value={25}>25</option>
                            <option value={50}>50</option>
                            <option value={100}>100</option>
                          </select>{" "}
                          entries
                        </label>
                      </div>
                    </div>
                    <div className="col-sm-12 col-md-6">
                      <div
                        id="example1_filter"
                        className={classNames(s.dataTablesFilter)}
                      >
                        <label>
                          Search:
                          <input
                            type="search"
                            className="form-control form-control-sm"
                            placeholder=""
                            value={keywordAllField || ""}
                            onChange={(e) => handleOnChange(e.target.value)}
                          />
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className={s.table}>
                    <div
                      ref={tableRef}
                      className={classNames("row", s.tableScrollable)}
                      style={{
                        maxHeight:
                          collapsible || maxHeightProps
                            ? maxHeightProps
                            : maxHeight,
                      }}
                    >
                      <div className="col-sm-12">
                        <div className="dataTables_scroll">
                          <div className="dataTables_scrollBody">
                            <table
                              className="table display product-overview mb-30"
                              id="support_table5"
                            >
                              <thead>
                                <tr>
                                  {table_th.map((obj) => {
                                    return obj.key === "_id" ? (
                                      <Fragment key={obj.key} />
                                    ) : (
                                      <th
                                        key={obj.key}
                                        className={classNames(
                                          {
                                            [s.sorting_asc]:
                                              sortAsc[obj.key] === true,
                                          },
                                          {
                                            [s.sorting_desc]:
                                              sortAsc[obj.key] === false,
                                          },
                                          s.sort,
                                          {
                                            [s.title]: obj.key === "title",
                                          },
                                          {
                                            [s.actions]:
                                              obj.key.includes("actions"),
                                          }
                                        )}
                                        onClick={() => handleSort(obj.key)}
                                      >
                                        {obj.label}
                                      </th>
                                    );
                                  })}
                                </tr>
                              </thead>
                              {error ? (
                                error
                              ) : (
                                <Body
                                  table_tr={table_tr}
                                  variant={variant}
                                  handleDelete={handleDelete}
                                  handleImageShow={handleImageShow}
                                />
                              )}
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={classNames("row", s.pagination)}>
                      <div className="col-sm-12 col-md-5">
                        <div className={s.dataTablesInfo} id="example1_info">
                          {`Showing ${min} to ${max} of ${totalItems} entries`}
                        </div>
                      </div>
                      <div className="col-sm-12 col-md-7">
                        <div
                          className={classNames(
                            s.dataTablesPaginate,
                            s.pagingSimpleNumbers
                          )}
                          id="example1_paginate"
                        >
                          <ul className="pagination">
                            <li
                              className={classNames(
                                "page-item",
                                s.pageItem,
                                s.previous,
                                { [s.disabled]: page === "1" }
                              )}
                            >
                              <Link
                                to={{
                                  search: addParams(location.search, {
                                    page:
                                      parseInt(page) > 1
                                        ? (parseInt(page) - 1).toString()
                                        : page,
                                  }),
                                }}
                                className="page-link"
                              >
                                Previous
                              </Link>
                            </li>

                            {arr.map((o) => (
                              <li
                                key={o}
                                className={classNames(s.pageItem, {
                                  [s.active]: o === page,
                                })}
                              >
                                <Link
                                  to={{
                                    search: addParams(location.search, {
                                      page: o,
                                    }),
                                  }}
                                  className="page-link"
                                >
                                  {o}
                                </Link>
                              </li>
                            ))}

                            <li
                              className={classNames(
                                "page-item",
                                s.pageItem,
                                s.next,
                                { [s.disabled]: page === arr.length.toString() }
                              )}
                            >
                              <Link
                                to={{
                                  search: addParams(location.search, {
                                    page:
                                      parseInt(page) < arr.length
                                        ? (parseInt(page) + 1).toString()
                                        : page,
                                  }),
                                }}
                                className="page-link"
                              >
                                Next
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Table;
