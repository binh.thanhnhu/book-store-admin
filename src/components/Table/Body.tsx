import Tooltip from "@material-ui/core/Tooltip";
import classNames from "classnames";
import { ROUTES, Status } from "constants/data";
import { keyRoutes } from "constants/interface";
import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import s from "./Body.module.css";

interface Props {
  table_tr: any;
  variant?: keyRoutes;
  handleDelete: (id: string) => void;
  handleImageShow: (url: string) => void;
}

const Body: React.FC<Props> = ({
  table_tr,
  variant,
  handleDelete,
  handleImageShow,
}: Props) => {
  return (
    <tbody className={s.root}>
      {table_tr.map((obj: any, i: number) => (
        <tr key={i}>
          {Object.keys(obj).map((itemName: string) => {
            if (itemName === "status") {
              return (
                <td key={itemName}>
                  <span
                    className={classNames(
                      "label",
                      s.label,
                      s.labelSm,
                      {
                        "label-success": Status.success.includes(
                          obj.status.value
                        ),
                      },
                      {
                        "label-danger": Status.danger.includes(
                          obj.status.value
                        ),
                      },
                      {
                        "label-warning": Status.warning.includes(
                          obj.status.value
                        ),
                      }
                    )}
                  >
                    {obj[itemName].label}
                  </span>
                </td>
              );
            } else if (itemName.includes("actions") && !!variant) {
              return (
                <td key={itemName} className={s.actions}>
                  {itemName.includes("info") && (
                    <Tooltip title="Xem" placement="top" arrow>
                      <Link
                        to={`${ROUTES[variant].data.view?.path}/${obj._id}`}
                        className={classNames(
                          "btn",
                          s.btnTblInfo,
                          s.btn,
                          s.btnXS
                        )}
                      >
                        <i className="material-icons">info</i>
                      </Link>
                    </Tooltip>
                  )}
                  {itemName.includes("edit") && (
                    <Tooltip title="Sửa" placement="top" arrow>
                      <Link
                        to={`${ROUTES[variant].data.update?.path}/${obj._id}`}
                        className={classNames(
                          "btn",
                          s.btnTblEdit,
                          s.btn,
                          s.btnXS
                        )}
                      >
                        <i className="material-icons">edit</i>
                      </Link>
                    </Tooltip>
                  )}
                  {itemName.includes("delete") && (
                    <Tooltip title="Xóa" placement="top" arrow>
                      <button
                        className={classNames(
                          "btn",
                          s.btnTblDelete,
                          s.btn,
                          s.btnXS
                        )}
                        onClick={() => handleDelete(obj._id)}
                      >
                        <i className="material-icons">delete_forever</i>
                      </button>
                    </Tooltip>
                  )}
                </td>
              );
            } else if (itemName === "imageFromUrl") {
              return (
                <td key={itemName} className={s.hasImage}>
                  <div
                    className={s.imageWrap}
                    onClick={() => handleImageShow(obj[itemName][0]?.url || "")}
                  >
                    <img
                      src={obj[itemName][0]?.url || ""}
                      alt=""
                      className={s.image}
                    />
                  </div>
                </td>
              );
            } else if (itemName === "title") {
              return (
                <td key={itemName}>
                  <div title={obj[itemName]} className={s.maxLines}>
                    {obj[itemName]}
                  </div>
                </td>
              );
            } else if (itemName === "price" || itemName === "oldPrice") {
              return (
                <td key={itemName}>
                  <div className={s.price}>{obj[itemName]}</div>
                </td>
              );
            } else if (itemName !== "_id") {
              return <td key={itemName}>{obj[itemName]}</td>;
            } else return <Fragment key={itemName} />;
          })}
        </tr>
      ))}
    </tbody>
  );
};

export default Body;
