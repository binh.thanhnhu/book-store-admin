import React, { forwardRef } from "react";

import s from "./SweetAlert.module.css";

interface Props {
  children: React.ReactNode;
}

const SweetAlert = ({ children }: Props, ref: any) => {
  return (
    <div className={s.root}>
      <div ref={ref} className={s.container}>
        {children}
      </div>
    </div>
  );
};

export default forwardRef(SweetAlert);
