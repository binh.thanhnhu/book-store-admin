import classNames from "classnames";
import React from "react";

import s from "./Searchbox.module.css";

const Searchbox = () => {
  return (
    <form className={s.root}>
      <div className="input-group">
        <input
          type="text"
          className={classNames("form-control", s.input)}
          placeholder="Search..."
          name="query"
        />
        <span className="input-group-btn">
          <a href="/#" className={classNames("btn", s.submit)}>
            <i className="material-icons">search</i>
          </a>
        </span>
      </div>
    </form>
  );
};

export default Searchbox;
