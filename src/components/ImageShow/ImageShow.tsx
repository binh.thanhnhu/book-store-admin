import React from "react";
import s from "./ImageShow.module.css";

interface Props {
  urlImage: string;
}

const ImageShow: React.FC<Props> = ({ urlImage }) => {
  return (
    <div className={s.root}>
      <img src={urlImage} alt="" />
    </div>
  );
};

export default ImageShow;
