import classNames from "classnames";
import useCollapse from "hooks/useCollapse";
import React, { createRef } from "react";
import s from "./Card.module.css";

interface Props {
  header?: string;
  children: React.ReactNode;
  shadow?: boolean;
  minHeight?: number;
  collapsible?: boolean;
}

const Card: React.FC<Props> = ({
  header,
  children,
  shadow,
  minHeight,
  collapsible,
}) => {
  const divRef = createRef<HTMLDivElement>();

  const { collapse, setCollapse, height } = useCollapse(divRef, 0);

  return (
    <div className="row">
      <div className="col-sm-12">
        <div
          ref={divRef}
          className={classNames(s.cardBox, { [s.shadow]: shadow })}
          style={minHeight ? (collapsible ? { height } : { minHeight }) : {}}
        >
          <div className={s.cardHead}>
            <header>{header || "Basic Information"}</header>
            {collapsible && (
              <button
                onClick={() => setCollapse(!collapse)}
                className="mdl-button mdl-js-button mdl-button--icon pull-right"
              >
                <i className="material-icons">expand_more</i>
              </button>
            )}
          </div>
          <div className={classNames(s.cardBody)}>{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Card;
