import classNames from "classnames";
import useCountUp from "hooks/useCountUp";
import React from "react";
import CountUp from "react-countup";
import s from "./StateOverview.module.css";
import formatPrice from "utils/formatPrice";

interface Props {
  icon: string;
  title: string;
  number: number;
  percent: number;
  background: string;
  money?: boolean;
}

const StateOverview: React.FC<Props> = ({
  icon,
  title,
  number,
  percent,
  background,
  money,
}: Props) => {
  const widthPercent = useCountUp(percent);

  return (
    <div className={classNames(s.root, "col-xl-4 col-md-6 col-12")}>
      <div className={classNames(background, s.infoBox)}>
        <span className="info-box-icon push-bottom">
          <i className="material-icons">{icon}</i>
        </span>
        <div className={s.infoBoxContent}>
          <span className={s.infoBoxText}>{title}</span>
          <span className={s.infoBoxNumber}>
            <CountUp
              end={number}
              duration={1}
              formattingFn={
                money ? (number) => `${formatPrice(number)} đ` : undefined
              }
              decimal=","
            />
          </span>
          <div className={classNames("progress", s.progress)}>
            <div
              className={classNames("progress-bar", s.progressBar)}
              style={{
                width: widthPercent + "%",
              }}
            />
          </div>
          <span className={s.progressDescription}>
            <CountUp end={percent} duration={1} />
            {"% Increase in 28 Days"}
          </span>
        </div>
      </div>
    </div>
  );
};

export default StateOverview;
