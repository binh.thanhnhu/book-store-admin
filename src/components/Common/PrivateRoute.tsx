import { useAppSelector } from "app/hooks";
import React from "react";
import { Redirect, RouteProps, Route } from "react-router-dom";

const PrivateRoute = (props: RouteProps) => {
  const token = useAppSelector((state) => state.auth.token);

  if (!token) return <Redirect to="/login" />;

  return <Route {...props} />;
};

export default PrivateRoute;
