import { useAppDispatch, useAppSelector } from "app/hooks";
import { history } from "app/store";
import classNames from "classnames";
import BackgroundLoading from "components/BackgroundLoading/BackgroundLoading";
import PrivateRoute from "components/Common/PrivateRoute";
import Header from "components/Header/Header";
import LoadingGlobal from "components/LoadingGlobal/LoadingGlobal";
import PageBar from "components/PageBar/PageBar";
import Sidebar from "components/Sidebar/Sidebar";
import { refreshAllMessagesRequest } from "features/global/globalSlice";
import useClickOutside from "hooks/useClickOutside";
import CreateNewCustomerPage from "pages/CustomerPage/CreateNewCustomerPage";
import CustomerInfomationPage from "pages/CustomerPage/CustomerInformationPage";
import UpdateCustomerPage from "pages/CustomerPage/UpdateCustomerPage";
import ViewAllCustomersPage from "pages/CustomerPage/ViewAllCustomersPage";
import HomePage from "pages/Homepage/HomePage";
import LoginPage from "pages/LoginPage/LoginPage";
import OrderInfomationPage from "pages/OrderPage/OrderInfomationPage";
import ViewAllOrdersPage from "pages/OrderPage/ViewAllOrdersPage";
import CreateNewProductPage from "pages/ProductPage/CreateNewProductPage";
import ProductInfomationPage from "pages/ProductPage/ProductInformationPage";
import UpdateProductPage from "pages/ProductPage/UpdateProductPage";
import ViewAllProductsPage from "pages/ProductPage/ViewAllProductsPage";
import CreateNewUserPage from "pages/UserPage/CreateNewUserPage";
import UpdateUserPage from "pages/UserPage/UpdateUserPage";
import UserInfomationPage from "pages/UserPage/UserInfomationPage";
import ViewAllUsersPage from "pages/UserPage/ViewAllUsersPage";
import React, { createRef, useEffect, useState } from "react";
import { Redirect, Route, Router, Switch } from "react-router-dom";
import s from "./App.module.css";

function App() {
  const [isSidebarOpen, setSidebarOpen] = useState<boolean>(false);
  const sidebarRef = createRef<HTMLDivElement>();
  const backgroundLoading = useAppSelector(
    (state) => state.global.backgroundLoading
  );
  const loading = useAppSelector((state) => state.global.loading);
  const isDarkMode = useAppSelector((state) => state.global.darkmode);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(refreshAllMessagesRequest());
  }, [dispatch]);

  const handleMobileSidebarOpen = () => {
    console.log(1111);
    setSidebarOpen(!isSidebarOpen);
  };

  useClickOutside(sidebarRef, isSidebarOpen, () => setSidebarOpen(false));

  return backgroundLoading ? (
    <BackgroundLoading />
  ) : (
    <div className={s.App}>
      {loading && <LoadingGlobal />}
      <div
        className={classNames(
          s.pageHeaderFixed,
          s.darkSidebarColor,
          { dark: isDarkMode },
          { light: !isDarkMode }
        )}
      >
        <div className={s.pageWrapper}>
          <Router history={history}>
            <Header handleMobileSidebarOpen={handleMobileSidebarOpen} />
            <div className={s.pageContainer}>
              <div className={classNames({ [s.darken]: isSidebarOpen })}></div>
              <div
                ref={sidebarRef}
                className={classNames(
                  { [s.sidebarOpen]: isSidebarOpen },
                  { [s.sidebarClose]: !isSidebarOpen }
                )}
              >
                <Sidebar />
              </div>
              <div className={s.pageContentWrapper}>
                <div className={s.pageContent}>
                  <PageBar />
                  <Switch>
                    <Route path="/login" component={LoginPage} />
                    {/*   */}
                    <Route exact path="/">
                      <Redirect to="/app/dashboard" />
                    </Route>
                    {/*   */}
                    <PrivateRoute path="/app/dashboard" component={HomePage} />
                    {/*  */}
                    <PrivateRoute
                      path="/app/user"
                      component={ViewAllUsersPage}
                    />
                    <PrivateRoute
                      path="/app/new-user"
                      component={CreateNewUserPage}
                    />
                    <PrivateRoute
                      path="/app/update-user/:id"
                      component={UpdateUserPage}
                    />
                    <PrivateRoute
                      path="/app/view-user/:id"
                      component={UserInfomationPage}
                    />
                    {/*  */}
                    <PrivateRoute
                      path="/app/customer"
                      component={ViewAllCustomersPage}
                    />
                    <PrivateRoute
                      path="/app/new-customer"
                      component={CreateNewCustomerPage}
                    />
                    <PrivateRoute
                      path="/app/update-customer/:id"
                      component={UpdateCustomerPage}
                    />
                    <PrivateRoute
                      path="/app/view-customer/:id"
                      component={CustomerInfomationPage}
                    />
                    {/*  */}
                    <PrivateRoute
                      path="/app/product"
                      component={ViewAllProductsPage}
                    />
                    <PrivateRoute
                      path="/app/new-product"
                      component={CreateNewProductPage}
                    />
                    <PrivateRoute
                      path="/app/update-product/:id"
                      component={UpdateProductPage}
                    />
                    <PrivateRoute
                      path="/app/view-product/:id"
                      component={ProductInfomationPage}
                    />
                    {/*  */}
                    <PrivateRoute
                      path="/app/order"
                      component={ViewAllOrdersPage}
                    />
                    <PrivateRoute
                      path="/app/update-order/:id"
                      component={OrderInfomationPage}
                    />
                  </Switch>
                </div>
              </div>
            </div>
          </Router>
        </div>
      </div>
    </div>
  );
}

export default App;
