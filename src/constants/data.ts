import { IRoutes } from "./interface";

export const ROUTES: IRoutes = {
  dashboard: {
    id: 0,
    title: "Bảng điều khiển",
    icon: "dashboard",
    data: {
      list: {
        id: 0,
        path: "/app/dashboard",
        title: "Dashboard",
      },
    },
  },
  user: {
    id: 1,
    title: "Nhân viên",
    icon: "group",
    data: {
      list: {
        id: 0,
        path: "/app/user",
        title: "Danh sách nhân viên",
      },
      new: {
        id: 1,
        path: "/app/new-user",
        title: "Thêm mới",
        role: "admin",
      },
      update: {
        id: 2,
        path: "/app/update-user",
        title: "Sửa thông tin",
        preventClick: true,
        role: "admin",
      },
      view: {
        id: 3,
        path: "/app/view-user",
        title: "Xem thông tin",
        preventClick: true,
      },
    },
  },
  customer: {
    id: 2,
    title: "Khách hàng",
    icon: "record_voice_over",
    data: {
      list: {
        id: 0,
        path: "/app/customer",
        title: "Danh sách khách hàng",
      },
      new: {
        id: 1,
        path: "/app/new-customer",
        title: "Thêm mới",
        role: "admin",
      },
      update: {
        id: 2,
        path: "/app/update-customer",
        title: "Sửa thông tin",
        preventClick: true,
        role: "admin",
      },
      view: {
        id: 3,
        path: "/app/view-customer",
        title: "Xem thông tin",
        preventClick: true,
      },
    },
  },
  product: {
    id: 3,
    title: "Sản phẩm",
    icon: "inventory",
    data: {
      list: {
        id: 0,
        path: "/app/product",
        title: "Danh sách sản phẩm",
      },
      new: {
        id: 1,
        path: "/app/new-product",
        title: "Thêm mới",
      },
      update: {
        id: 2,
        path: "/app/update-product",
        title: "Sửa thông tin",
        preventClick: true,
      },
      view: {
        id: 3,
        path: "/app/view-product",
        title: "Xem thông tin",
        preventClick: true,
      },
    },
  },
  order: {
    id: 4,
    title: "Đơn hàng",
    icon: "task",
    data: {
      list: {
        id: 0,
        path: "/app/order",
        title: "Đơn đặt hàng",
      },
      update: {
        id: 1,
        path: "/app/update-order",
        title: "Sửa thông tin",
        preventClick: true,
      },
    },
  },
};

export const products_th_info = [
  { key: "title", label: "Tiêu đề" },
  { key: "type", label: "Loại sách" },
  { key: "option", label: "Danh mục" },
  { key: "price", label: "Giá" },
  { key: "oldPrice", label: "Giá cũ" },
  { key: "author", label: "Tác giả" },
  { key: "SKU", label: "SKU" },
  { key: "categories", label: "Danh mục" },
  { key: "description", label: "Giới thiệu" },
  { key: "description2", label: "Mô tả" },
  { key: "rated", label: "Rating" },
  { key: "amount", label: "Số lượng" },
  { key: "numberOfPages", label: "Số trang" },
  { key: "publishingCompany", label: "Nhà xuất bản" },
  { key: "dateOfPublication", label: "Ngày xuất bản" },
  { key: "size", label: "Khổ giấy" },
  { key: "tags", label: "Tags" },
  { key: "imageFromUrl", label: "Hình ảnh" },
];

export const products_th = [
  { key: "_id", label: "ID" },
  { key: "no", label: "STT" },
  { key: "imageFromUrl", label: "Hình ảnh" },
  { key: "title", label: "Tiêu đề" },
  { key: "type", label: "Loại sản phẩm" },
  { key: "option", label: "Danh mục" },
  { key: "price", label: "Giá" },
  { key: "oldPrice", label: "Giá cũ" },
  { key: "author", label: "Tác giả" },
  { key: "SKU", label: "SKU" },
  { key: "amount", label: "Số lượng" },
  { key: "publishingCompany", label: "Nhà xuất bản" },
  { key: ["actions", "info", "edit", "delete"].toString(), label: "Hành động" },
];

export const customers_th = [
  { key: "_id", label: "ID" },
  { key: "no", label: "STT" },
  { key: "username", label: "Username" },
  { key: "firstName", label: "Tên" },
  { key: "lastName", label: "Họ" },
  { key: "email", label: "Email" },
  { key: "company", label: "Công ty" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: ["actions", "info", "edit", "delete"].toString(), label: "Hành động" },
];

export const customers_th_info = [
  { key: "username", label: "Tài khoản" },
  { key: "firstName", label: "Tên" },
  { key: "lastName", label: "Họ" },
  { key: "email", label: "Email" },
  { key: "company", label: "Công ty" },
  { key: "phone", label: "Số điện thoại" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: "postcode", label: "Postcode" },
];

export const users_th = [
  { key: "_id", label: "ID" },
  { key: "no", label: "STT" },
  { key: "username", label: "Username" },
  { key: "firstName", label: "Tên" },
  { key: "lastName", label: "Họ" },
  { key: "email", label: "Email" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: "role", label: "Chức vụ" },
  { key: ["actions", "info", "edit", "delete"].toString(), label: "Hành động" },
];

export const users_th_info = [
  { key: "username", label: "Tài khoản" },
  { key: "firstName", label: "Tên" },
  { key: "lastName", label: "Họ" },
  { key: "email", label: "Email" },
  { key: "phone", label: "Số điện thoại" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: "postcode", label: "Postcode" },
  { key: "role", label: "Chức vụ" },
];

export const orders_th = [
  { key: "_id", label: "ID" },
  { key: "no", label: "STT" },
  { key: "email", label: "Email" },
  { key: "username", label: "Username" },
  { key: "totalPrice", label: "Tổng tiền" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: "status", label: "Trạng thái" },
  { key: ["actions", "edit", "delete"].toString(), label: "Hành động" },
];

export const orders_th_info = [
  { key: "email", label: "Email" },
  { key: "username", label: "Username" },
  { key: "firstName", label: "Tên" },
  { key: "lastName", label: "Họ" },
  {
    key: "cart",
    label: "Cart",
    child: [
      {
        key: "info",
        data: [
          { key: "productTitle", label: "Tiêu đề" },
          { key: "SKU", label: "SKU" },
          { key: "price", label: "Đơn giá" },
          { key: "quantity", label: "Số lượng" },
        ],
      },
      { key: "totalPrice", label: "Tổng tiền" },
      { key: "time", label: "Thời gian đặt hàng" },
    ],
  },
  { key: "phone", label: "Số điện thoại" },
  { key: "country", label: "Quốc gia" },
  { key: "address", label: "Địa chỉ" },
  { key: "postcode", label: "Postcode" },
  { key: "status", label: "Tình trạng" },
];

export const table = {
  th: [
    { key: "no", label: "No" },
    { key: "name", label: "Name" },
    { key: "checkIn", label: "Check In" },
    { key: "checkOut", label: "Check Out" },
    { key: "status", label: "Status" },
    { key: "phone", label: "Phone" },
    { key: "roomType", label: "Room Type" },
    { key: "actions", label: "Edit" },
  ],
  tr: [
    {
      id: 1,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 2,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 3,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 4,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 5,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 6,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 7,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 8,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 9,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 10,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 11,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 12,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 13,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
    {
      id: 14,
      no: 1,
      name: "Jens Brincker",
      checkIn: "23/05/2016",
      checkOut: "27/05/2016",
      status: "paid",
      phone: "123456789",
      roomType: "Single",
      edit: true,
    },
  ],
};

export const Status = {
  warning: ["pending"],
  success: ["verified"],
  danger: ["cancelled"],
};

export const LabelStatus = [
  { value: "pending", label: "Đang chờ" },
  { value: "verified", label: "Đã duyệt" },
  { value: "cancelled", label: "Đã hủy" },
];

export const TypeProduct = [
  { value: "VietNameseBook", label: "Sách Việt Nam" },
  { value: "ForeignBook", label: "Sách nước ngoài" },
  { value: "Souvenir", label: "Quà lưu niệm" },
  { value: "Stationery", label: "Văn phòng phẩm" },
  { value: "Ebook", label: "Ebook" },
];

export const OptionVietNameseBook = [
  { value: "Literature", label: "Sách văn học" },
  { value: "Economic", label: "Sách kinh tế" },
  { value: "ChildrensBooks", label: "Sách cho trẻ em" },
  { value: "LifeSkill", label: "Sách kỹ năng sống" },
  { value: "Textbook", label: "Sách giáo khoa" },
  { value: "Comic", label: "Truyện tranh" },
  { value: "ReligionAndSpirituality", label: "Sách tôn giáo - tâm linh" },
  { value: "Journal", label: "Tạp chí" },
];

export const OptionForeignBook = [
  { value: "Cookbooks", label: "Cookbooks" },
  { value: "ScienceAndTechnology", label: "Science & Technology" },
  { value: "ParentingAndRelationships", label: "Parenting & Relationships" },
  { value: "Magazines", label: "Magazines" },
  { value: "Dictionary", label: "Dictionary" },
  { value: "SelfHelp", label: "Self Help" },
  { value: "ArtAndPhotography", label: "Art & Photography" },
  { value: "ChildrensBooks", label: "Children's Books" },
];

export const OptionUserRole = [
  { value: "admin", label: "Admin" },
  { value: "staff", label: "Nhân viên" },
];

export const NotificationType = [
  { value: "success", icon: "done", backgroundColor: "blue-bgcolor" },
  { value: "error", icon: "warning_amber", backgroundColor: "red-bgcolor" },
  { value: "info", icon: "feed", backgroundColor: "blue-bgcolor" },
];

export const DProduct = {
  type: "",
  option: "",
  title: "",
  author: "",
  price: 0,
  salePrice: 0,
  amount: 0,
  publishingCompany: "",
  SKU: "",
  numberOfPages: 0,
  size: "",
  dateOfPublication: null,
  description: "",
  description2: "",
  tags: "",
};

export const DCustomer = {
  username: "",
  password: "",
  firstName: "",
  lastName: "",
  company: "",
  country: "",
  address: "",
  postcode: "",
  phone: "",
  email: "",
};

export const DUser = {
  username: "",
  password: "",
  rePassword: "",
  firstName: "",
  lastName: "",
  country: "",
  address: "",
  postcode: "",
  phone: "",
  email: "",
  role: "staff",
};
