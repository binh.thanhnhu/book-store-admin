import * as Yup from "yup";

export const VStaffDetails = Yup.object({
  username: Yup.string().required("Vui lòng nhập tên đăng nhập"),
  firstName: Yup.string().required("Vui lòng nhập tên"),
  lastName: Yup.string().required("Vui lòng nhập họ"),
  // country: Yup.string().required("Vui lòng chọn quốc gia"),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  phone: Yup.string().required("Vui lòng nhập số điện thoại"),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
});

export const VProduct = Yup.object({
  type: Yup.string()
    .oneOf(["VietNameseBook", "ForeignBook", "Souvenir", "Stationery", "Ebook"])
    .required("Vui lòng chọn danh mục sản phẩm"),
  option: Yup.string()
    .when("type", {
      is: "VietNameseBook",
      then: Yup.string()
        .oneOf(
          [
            "Literature",
            "Economic",
            "ChildrensBooks",
            "LifeSkill",
            "Textbook",
            "Comic",
            "ReligionAndSpirituality",
            "Journal",
          ],
          "Vui lòng chọn đúng loại sách"
        )
        .required("Vui lòng chọn loại sách"),
    })
    .when("type", {
      is: "ForeignBook",
      then: Yup.string()
        .oneOf(
          [
            "Cookbooks",
            "ScienceAndTechnology",
            "ParentingAndRelationships",
            "Magazines",
            "Dictionary",
            "SelfHelp",
            "ArtAndPhotography",
            "ChildrensBooks",
          ],
          "Vui lòng chọn đúng loại sách"
        )
        .required("Vui lòng chọn loại sách"),
      otherwise: Yup.string(),
    }),
  title: Yup.string().required("Vui lòng nhập tiêu đề"),
  author: Yup.string().required("Vui lòng nhập tên tác giả"),
  tempPrice: Yup.number().required("Vui lòng nhập giá"),
  salePrice: Yup.number().lessThan(
    Yup.ref("tempPrice"),
    "Giá khuyến mãi phải nhỏ hơn giá"
  ),
  amount: Yup.number().required("Vui lòng nhập số lượng"),
  publishingCompany: Yup.string().required("Vui lòng nhập tên nhà sản xuất"),
  SKU: Yup.string().required("Vui lòng nhập mã SKU"),
  numberOfPages: Yup.number(),
  size: Yup.string(),
  dateOfPublication: Yup.date().typeError("Vui lòng nhập ngày sản xuất"),
  description: Yup.string().required("Vui lòng nhập mô tả"),
  description2: Yup.string(),
  tags: Yup.array().of(Yup.string()),
});

export const VCustomer = Yup.object({
  username: Yup.string().required("Vui lòng nhập tên đăng nhập"),
  password: Yup.string().required("Vui lòng nhập mật khẩu"),
  firstName: Yup.string().required("Vui lòng nhập tên"),
  lastName: Yup.string().required("Vui lòng nhập họ"),
  country: Yup.string(),
  company: Yup.string(),
  postcode: Yup.string(),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  phone: Yup.string(),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
});

export const VCustomerUpdate = Yup.object({
  firstName: Yup.string().required("Vui lòng nhập tên"),
  lastName: Yup.string().required("Vui lòng nhập họ"),
  country: Yup.string(),
  company: Yup.string(),
  postcode: Yup.string(),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  phone: Yup.string(),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
});

export const VUser = Yup.object({
  username: Yup.string().required("Vui lòng nhập tên đăng nhập"),
  password: Yup.string().required("Vui lòng nhập mật khẩu"),
  rePassword: Yup.string()
    .oneOf([Yup.ref("password"), null], "Mật khẩu không trùng khớp")
    .required("Vui lòng xác nhận mật khẩu"),
  firstName: Yup.string().required("Vui lòng nhập tên"),
  lastName: Yup.string().required("Vui lòng nhập họ"),
  country: Yup.string(),
  postcode: Yup.string(),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  phone: Yup.string(),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
  role: Yup.string()
    .oneOf(["admin", "staff"])
    .required("Vui lòng chọn chức vụ"),
});

export const VUserUpdate = Yup.object({
  firstName: Yup.string().required("Vui lòng nhập tên"),
  lastName: Yup.string().required("Vui lòng nhập họ"),
  country: Yup.string(),
  postcode: Yup.string(),
  address: Yup.string().required("Vui lòng nhập địa chỉ"),
  phone: Yup.string(),
  email: Yup.string()
    .email("Email không hợp lệ")
    .required("Vui lòng nhập email"),
  role: Yup.string()
    .oneOf(["admin", "staff"])
    .required("Vui lòng chọn chức vụ"),
});
