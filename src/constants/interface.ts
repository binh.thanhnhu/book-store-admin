export type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

export type keyRoutes = "dashboard" | "user" | "customer" | "product" | "order";
// | "warehouse";

export type IRoutes = {
  [key in keyRoutes]: {
    id: number;
    title: string;
    icon: string;
    data: {
      list?: {
        id: number;
        path: string;
        title: string;
        role?: IUser["role"];
      };
      new?: {
        id: number;
        path: string;
        title: string;
        role?: IUser["role"];
      };
      update?: {
        id: number;
        path: string;
        title: string;
        preventClick?: boolean;
        role?: IUser["role"];
      };
      view?: {
        id: number;
        path: string;
        title: string;
        preventClick?: boolean;
        role?: IUser["role"];
      };
      detail?: {
        id: number;
        path: string;
        title: string;
        role?: IUser["role"];
      };
    };
  };
};

export interface IProduct {
  _id: string;
  type: string;
  option?: string;
  categories?: string[];
  imageFromUrl: {
    public_id: string;
    url: string;
    fileSize: number;
  }[];
  image: {
    source: string | ArrayBuffer | null;
    info: IFileInfo | null;
    id: string;
  }[];
  title: string;
  author?: string;
  price: number;
  oldPrice?: number;
  rated?: number;
  description?: string;
  description2?: string;
  SKU?: string;
  customerReviews?: number;
  amount?: number;
  publishingCompany?: string;
  numberOfPages?: number;
  size?: string;
  dateOfPublication?: Date;
  createdAt?: Date;
  creatorId: string;
  tag?: string[];
}

export interface ICustomer {
  _id: string;
  username: string;
  firstName: string;
  lastName: string;
  company?: string;
  country: string;
  address: string;
  postcode?: string;
  phone?: string;
  email: string;
  createdAt?: Date;
  password?: string;
}

export interface IUser {
  _id: string;
  username: string;
  firstName: string;
  lastName: string;
  country: string;
  address: string;
  postcode?: string;
  phone?: string;
  email: string;
  role: "admin" | "staff";
  createdAt?: Date;
  password?: string;
}

export interface IOrder {
  _id: string;
  customerId: string;
  username: string;
  cart: {
    info: {
      productId: string;
      productTitle: string;
      price: number;
      quantity: number;
    }[];
    time: Date;
    totalPrice: number;
  };
  firstName: string;
  lastName: string;
  company?: string;
  country?: string;
  address: string;
  postcode?: string;
  phone: string;
  orderComments?: string;
  email: string;
  status: "pending" | "verified" | "cancelled";
  createdAt?: Date;
}

export interface ILogin {
  username: string;
  password: string;
}

export interface IResponse<T> {
  success: boolean;
  message: string;
  data: T;
}

export interface IResponseLogin<T> {
  success: boolean;
  message: string;
  token: string;
  data: T;
}

export interface IParam {
  type?: string;
  option?: string;
  page?: string;
  sort?: string;
  sortName?: string;
  keyword?: string;
  perPage?: string;
  keywordAllField?: string;
  keywordAllFieldFix?: string;
}

export interface ICategory {
  category: IProduct[];
  totalPages: number;
  totalProducts: number;
}

export interface IFileInfo {
  fileSize: number;
  fileType: string;
}

export interface INotification {
  option: "success" | "warning" | "error" | "info";
  message: string;
  time: string;
  seen: boolean;
  path?: string;
}

export interface IMessage {
  option: "success" | "warning" | "error" | "info";
  message: string;
  time: string;
  seen: boolean;
  _id: string;
  suffix?: string;
  route?: keyRoutes;
  path?: string;
}

export interface IStatistics {
  totalSales: number;
  totalSalesMoney: number;
  totalCustomers: number;
  totalOrders: number;
  totalVisitsSite: number;
  totalVietnameseBooks: number;
  totalForeignBooks: number;
  totalStationeries: number;
  totalSouvenirs: number;
  totalEbooks: number;
}

export interface ITotal {
  value: string;
  title: string;
  number: number;
  icon: string;
  percent: number;
  background: string;
  money?: boolean;
}
