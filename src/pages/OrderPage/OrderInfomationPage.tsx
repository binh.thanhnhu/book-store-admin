import { useAppDispatch, useAppSelector } from "app/hooks";
import Information from "components/Infomation/Information";
import { LabelStatus, orders_th_info } from "constants/data";
import { IOrder } from "constants/interface";
import {
  changeStatusOrderRequest,
  getOrderRequest,
} from "features/order/orderSlice";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const OrderInfomationPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const order = useAppSelector((state) => state.order.order);
  const dispatch = useAppDispatch();

  let temp: any = {};
  let orderMap: any = {};
  let statusMap: any = {};
  LabelStatus.forEach((el) => (statusMap[el.value] = el));

  orders_th_info.forEach((item) => {
    orderMap[item.key] = item;
    if (item.key === "status") {
      temp[item.key] = order && statusMap[order.status].label;
      return;
    }
    temp[item.key] = order && order[item.key as keyof IOrder];
  });

  useEffect(() => {
    dispatch(getOrderRequest(id));
  }, [dispatch, id]);

  const handleStatusChange = (status: IOrder["status"]): void => {
    dispatch(changeStatusOrderRequest({ id, status }));
  };

  return (
    <>
      <Information
        status={order?.status}
        item={temp}
        itemMap={orderMap}
        order
        handleStatusChange={handleStatusChange}
      />
    </>
  );
};

export default OrderInfomationPage;
