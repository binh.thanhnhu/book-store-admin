import { Button } from "@material-ui/core";
import { useAppDispatch, useAppSelector } from "app/hooks";
import SweetAlert from "components/SweetAlert/SweetAlert";
import Table from "components/Table/Table";
import { orders_th } from "constants/data";
import {
  deleteOrderRequest,
  getAllOrdersByOptionRequest,
} from "features/order/orderSlice";
import useClickOutside from "hooks/useClickOutside";
import useDebounce from "hooks/useDebounce";
import useFilterField from "hooks/useFilterField";
import React, { createRef, useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import addParams from "utils/addParams";
import getParams from "utils/getParams";

interface Props {
  height?: number;
  collapsible?: boolean;
}

const ViewAllOrdersPage: React.FC<Props> = ({ height, collapsible }) => {
  const [_page, setPage] = useState<string>("1");
  const [_perPage, setPerPage] = useState<string>("12");
  const [_arr, setArr] = useState<string[]>([]);
  const [_sortAsc, setSortAsc] = useState<{ [key: string]: boolean }>({
    no: true,
  });
  const [keywordAllField, setKeywordAllField] = useState<string>("");
  const [orderDelete, setOrderDelete] = useState<any>({});
  const ref = createRef();
  const error = useAppSelector(
    (state) => state.order.error.getAllOrdersByOption
  );
  const loading = useAppSelector((state) => state.order.loading);
  const orders = useAppSelector((state) => state.order.orders);
  const totalPages =
    useAppSelector((state) => state.order.totalPages) ||
    Math.ceil(orders.length / 12);
  const totalOrders = useAppSelector((state) => state.order.totalOrders);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const history = useHistory();
  const debounce = useDebounce();

  const { array } = useFilterField(orders, totalOrders, orders_th, "order");
  useClickOutside(ref, orderDelete.id, () => setOrderDelete({}));

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    const params = getParams(location.search);
    const { page, perPage, keywordAllFieldFix } = params;

    if (Object.keys(params).length > 0)
      dispatch(getAllOrdersByOptionRequest(params));
    else {
      dispatch(getAllOrdersByOptionRequest({ page: "1", perPage: "12" }));
    }

    setPerPage(perPage || "12");
    setPage(page || "1");
    setKeywordAllField(keywordAllFieldFix);
  }, [location, dispatch]);

  useEffect(() => {
    const arr: string[] = [];
    for (let i = 1; i <= totalPages; i++) {
      arr.push(i.toString());
    }
    setArr(arr);
  }, [totalPages]);

  const handlePerPage = (perPage: string): void => {
    setPerPage(perPage);
    history.push({ search: addParams(location.search, { perPage }) });
  };

  const handleSort = (key: string): void => {
    setSortAsc({ [key]: !_sortAsc[key] });
    history.push({
      search: addParams(location.search, {
        sortName: key === "no" ? "createdAt" : key,
        sort: _sortAsc[key] ? "-1" : "1",
      }),
    });
  };

  const handleOnChange = (value: string): void => {
    setKeywordAllField(value);
    debounce<string>(value, 700, getKeyword);
  };

  const handleDelete = (id: string): void => {
    const order = orders.filter((order) => order._id === id)[0];
    setOrderDelete({
      id,
      username: order.username,
      email: order.email,
    });
  };

  const deleteOrder = (id: string): void => {
    dispatch(deleteOrderRequest(id));
    setOrderDelete({});
  };

  const handleTableReset = (): void => {
    const params = getParams(location.search);
    const { page, perPage } = params;

    if (Object.keys(params).length > 0)
      dispatch(getAllOrdersByOptionRequest(params));
    else {
      dispatch(getAllOrdersByOptionRequest({ page: "1", perPage: "12" }));
    }

    setPerPage(perPage || "12");
    setPage(page || "1");
  };

  const getKeyword = (value: string): void => {
    history.push({
      search: addParams(
        location.search,
        {
          page: "1",
          keywordAllField: encodeURIComponent(value).replace(
            /%26/g,
            "%jsfei026"
          ),
          keywordAllFieldFix: encodeURIComponent(value),
        },
        "all"
      ),
    });
  };

  return (
    <>
      {orderDelete.id && (
        <SweetAlert ref={ref}>
          <>
            <h6>
              {`Bạn có chắc chắn muốn xóa đơn hàng của khách hàng ${orderDelete.username} (${orderDelete.email}) ?`}
            </h6>
            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-t-10 m-r-10 m-l-10 m-b-10"
                onClick={() => deleteOrder(orderDelete.id)}
              >
                Submit
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-t-10 m-r-10 m-l-10 m-b-10"
                onClick={() => setOrderDelete({})}
              >
                Cancel
              </Button>
            </div>
          </>
        </SweetAlert>
      )}
      <Table
        title="Danh sách đơn hàng"
        showTitle
        maxHeightProps={height}
        collapsible={collapsible}
        table_th={orders_th}
        table_tr={array}
        arr={_arr}
        minWidth={1300}
        perPage={_perPage}
        page={_page}
        totalItems={totalOrders}
        sortAsc={_sortAsc}
        keywordAllField={keywordAllField}
        loading={loading}
        variant="order"
        error={error}
        handlePerPage={handlePerPage}
        handleSort={handleSort}
        handleOnChange={handleOnChange}
        handleDelete={handleDelete}
        handleTableReset={handleTableReset}
      />
    </>
  );
};

export default ViewAllOrdersPage;
