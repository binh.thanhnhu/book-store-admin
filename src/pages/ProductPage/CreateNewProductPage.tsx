import MenuItem from "@material-ui/core/MenuItem";
import { useAppDispatch, useAppSelector } from "app/hooks";
import Button from "components/Button/Button";
import Card from "components/Card/Card";
import FileUpload from "components/FileUpload/FileUpload";
import TextField from "components/TextField/TextField";
import {
  DProduct,
  OptionForeignBook,
  OptionVietNameseBook,
  TypeProduct,
} from "constants/data";
import { IProduct } from "constants/interface";
import { VProduct } from "constants/validation";
import { createNewProductRequest } from "features/product/productSlice";
import { FastField, Field, Form, Formik } from "formik";
import moment from "moment";
import React, { useState } from "react";

const CreateNewProductPage: React.FC = () => {
  const [images, setImages] = useState<IProduct["image"]>();
  const [imagesQuantity, setImagesQuantity] = useState<number>(0);
  const user = useAppSelector((state) => state.auth.user);
  const dispatch = useAppDispatch();

  const handleSubmit = (values: any) => {
    if (values.dateOfPublication) {
      values.dateOfPublication = moment(values.dateOfPublication).format(
        "YYYY-MM-DD"
      );
    }
    if (values.salePrice) {
      values.oldPrice = JSON.parse(JSON.stringify(values.tempPrice));
      values.price = JSON.parse(JSON.stringify(values.salePrice));
    } else {
      values.price = JSON.parse(JSON.stringify(values.tempPrice));
      values.oldPrice = null;
    }
    dispatch(
      createNewProductRequest({
        ...values,
        creatorId: user?._id,
        image: images,
        imagesQuantity,
      })
    );
  };

  const handleImagesSource = (
    image: IProduct["image"],
    _: string[],
    imagesQuantity: { quantity: number; totalSize: number }
  ) => {
    setImagesQuantity(imagesQuantity.quantity);
    setImages(image);
  };

  const renderOptionField = (value: string): React.ReactNode => {
    switch (value) {
      case "VietNameseBook":
        return (
          <div className="col-lg-6 p-t-24">
            <Field component={TextField} label="Loại sách" select name="option">
              {OptionVietNameseBook.map((option: any) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Field>
          </div>
        );
      case "ForeignBook":
        return (
          <div className="col-lg-6 p-t-24">
            <Field component={TextField} label="Loại sách" select name="option">
              {OptionForeignBook.map((option: any) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </Field>
          </div>
        );
      default:
        return;
    }
  };

  return (
    <Card>
      <Formik
        onSubmit={handleSubmit}
        initialValues={DProduct}
        validationSchema={VProduct}
      >
        {(formik) => (
          <Form className="row">
            <div className="col-lg-6 p-t-24">
              <Field
                component={TextField}
                label="Danh mục sản phẩm"
                select
                name="type"
              >
                {TypeProduct.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </Field>
            </div>

            {renderOptionField(formik.values.type)}

            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tiêu đề" name="title" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tác giả" name="author" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Giá" name="tempPrice" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Giá khuyến mãi"
                name="salePrice"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Số lượng" name="amount" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Nhà phát hành"
                name="publishingCompany"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="SKU" name="SKU" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Số trang"
                name="numberOfPages"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Khổ giấy" name="size" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Ngày sản xuất"
                type="date"
                name="dateOfPublication"
              />
            </div>
            <div className="col-lg-12 p-t-24">
              <FastField
                component={TextField}
                label="Mô tả"
                name="description"
                multiline
                rowsMax={4}
              />
            </div>
            <div className="col-lg-12 p-t-24">
              <FastField
                component={TextField}
                label="Mô tả chi tiết"
                name="description2"
                multiline
                rowsMax={10}
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tag" name="tags" />
            </div>

            <div className="col-lg-12 p-t-24">
              <FileUpload handleImagesSource={handleImagesSource} />
            </div>

            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              >
                Submit
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
                onClick={() => formik.resetForm()}
              >
                Cancel
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Card>
  );
};

export default CreateNewProductPage;
