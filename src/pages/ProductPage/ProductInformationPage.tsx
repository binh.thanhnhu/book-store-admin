import { useAppDispatch, useAppSelector } from "app/hooks";
import Information from "components/Infomation/Information";
import {
  OptionForeignBook,
  OptionVietNameseBook,
  products_th_info,
  TypeProduct,
} from "constants/data";
import { IProduct } from "constants/interface";
import { getProductRequest } from "features/product/productSlice";
import moment from "moment";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const ProductInfomationPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const product = useAppSelector((state) => state.product.product);
  const dispatch = useAppDispatch();

  let temp: any = {};
  let productMap: any = {};
  let typeProductMap: any = {};
  let optionVietNameseBookMap: any = {};
  let optionForeignBookMap: any = {};
  TypeProduct.forEach((el) => (typeProductMap[el.value] = el));
  OptionVietNameseBook.forEach(
    (el) => (optionVietNameseBookMap[el.value] = el)
  );
  OptionForeignBook.forEach((el) => (optionForeignBookMap[el.value] = el));

  products_th_info.forEach((item) => {
    productMap[item.key] = item;
    if (item.key === "dateOfPublication") {
      temp[item.key] =
        product &&
        moment(product["dateOfPublication"]).format("MM-DD-YYYY") +
          " (MM-DD-YYYY)";
      return;
    }
    if (item.key === "type") {
      temp[item.key] =
        product && typeProductMap[product.type as keyof IProduct]?.label;
      return;
    }
    if (item.key === "option") {
      temp[item.key] = (
        product?.type === "VietNameseBook"
          ? optionVietNameseBookMap
          : optionForeignBookMap
      )[product?.option as keyof IProduct]?.label;
      return;
    }
    temp[item.key] = product && product[item.key as keyof IProduct];
  });

  useEffect(() => {
    dispatch(getProductRequest(id));
  }, [dispatch, id]);

  return <Information item={temp} itemMap={productMap} />;
};

export default ProductInfomationPage;
