import MenuItem from "@material-ui/core/MenuItem";
import { useAppDispatch } from "app/hooks";
import Button from "components/Button/Button";
import Card from "components/Card/Card";
import TextField from "components/TextField/TextField";
import { DUser, OptionUserRole } from "constants/data";
import { VUser } from "constants/validation";
import { createNewUserRequest } from "features/user/userSlice";
import { FastField, Form, Formik } from "formik";
import React from "react";

const CreateNewUserPage: React.FC = () => {
  const dispatch = useAppDispatch();

  const handleSubmit = (values: any) => {
    dispatch(createNewUserRequest(values));
  };

  return (
    <Card>
      <Formik
        onSubmit={handleSubmit}
        initialValues={DUser}
        validationSchema={VUser}
      >
        {(formik) => (
          <Form className="row">
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Tài khoản"
                name="username"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                type="password"
                label="Mật khẩu"
                name="password"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                type="password"
                label="Nhập lại mật khẩu"
                name="rePassword"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tên" name="firstName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Họ" name="lastName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Số điện thoại"
                name="phone"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Email" name="email" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Chức vụ"
                select
                name="role"
              >
                {OptionUserRole.map((option: any) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </FastField>
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Quốc gia"
                name="country"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Postcode"
                name="postcode"
              />
            </div>
            <div className="col-lg-12 p-t-24">
              <FastField
                component={TextField}
                label="Địa chỉ"
                name="address"
                multiline
                rowsMax={4}
              />
            </div>

            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              >
                Xác nhận
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
                onClick={() => formik.resetForm()}
              >
                Hủy bỏ
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Card>
  );
};

export default CreateNewUserPage;
