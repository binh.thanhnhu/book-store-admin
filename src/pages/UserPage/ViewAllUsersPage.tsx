import { Button } from "@material-ui/core";
import { useAppDispatch, useAppSelector } from "app/hooks";
import SweetAlert from "components/SweetAlert/SweetAlert";
import Table from "components/Table/Table";
import { users_th } from "constants/data";
import {
  deleteUserRequest,
  getAllUsersByOptionRequest,
} from "features/user/userSlice";
import useClickOutside from "hooks/useClickOutside";
import useDebounce from "hooks/useDebounce";
import useFilterField from "hooks/useFilterField";
import React, { createRef, useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import addParams from "utils/addParams";
import getParams from "utils/getParams";

const ViewAllUsers: React.FC = () => {
  const [_page, setPage] = useState<string>("1");
  const [_perPage, setPerPage] = useState<string>("12");
  const [_arr, setArr] = useState<string[]>([]);
  const [_sortAsc, setSortAsc] = useState<{ [key: string]: boolean }>({
    no: true,
  });
  const [keywordAllField, setKeywordAllField] = useState<string>("");
  const [userDelete, setUserDelete] = useState<any>({});
  const ref = createRef();
  const error = useAppSelector((state) => state.user.error.getAllUsersByOption);
  const loading = useAppSelector((state) => state.user.loading);
  const users = useAppSelector((state) => state.user.users);
  const totalPages =
    useAppSelector((state) => state.user.totalPages) ||
    Math.ceil(users.length / 12);
  const totalUsers = useAppSelector((state) => state.user.totalUsers);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const history = useHistory();
  const debounce = useDebounce();

  const { array } = useFilterField(users, totalUsers, users_th, "user");
  useClickOutside(ref, userDelete.id, () => setUserDelete({}));

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    const params = getParams(location.search);
    const { page, perPage, keywordAllFieldFix } = params;

    if (Object.keys(params).length > 0)
      dispatch(getAllUsersByOptionRequest(params));
    else {
      dispatch(getAllUsersByOptionRequest({ page: "1", perPage: "12" }));
    }

    setPerPage(perPage || "12");
    setPage(page || "1");
    setKeywordAllField(keywordAllFieldFix);
  }, [location, dispatch]);

  useEffect(() => {
    const arr: string[] = [];
    for (let i = 1; i <= totalPages; i++) {
      arr.push(i.toString());
    }
    setArr(arr);
  }, [totalPages]);

  const handlePerPage = (perPage: string): void => {
    setPerPage(perPage);
    history.push({ search: addParams(location.search, { perPage }) });
  };

  const handleSort = (key: string): void => {
    setSortAsc({ [key]: !_sortAsc[key] });
    history.push({
      search: addParams(location.search, {
        sortName: key === "no" ? "createdAt" : key,
        sort: _sortAsc[key] ? "-1" : "1",
      }),
    });
  };

  const handleOnChange = (value: string): void => {
    setKeywordAllField(value);
    debounce<string>(value, 700, getKeyword);
  };

  const handleDelete = (id: string): void => {
    const user = users.filter((user) => user._id === id)[0];
    setUserDelete({
      id,
      username: user.username,
      email: user.email,
    });
  };

  const deleteUser = (id: string): void => {
    dispatch(deleteUserRequest(id));
    setUserDelete({});
  };

  const handleTableReset = (): void => {
    const params = getParams(location.search);
    const { page, perPage } = params;

    if (Object.keys(params).length > 0)
      dispatch(getAllUsersByOptionRequest(params));
    else {
      dispatch(getAllUsersByOptionRequest({ page: "1", perPage: "12" }));
    }

    setPerPage(perPage || "12");
    setPage(page || "1");
  };

  const getKeyword = (value: string): void => {
    history.push({
      search: addParams(
        location.search,
        {
          page: "1",
          keywordAllField: encodeURIComponent(value).replace(
            /%26/g,
            "%jsfei026"
          ),
          keywordAllFieldFix: encodeURIComponent(value),
        },
        "all"
      ),
    });
  };

  return (
    <>
      {userDelete.id && (
        <SweetAlert ref={ref}>
          <>
            <h6>
              {`Bạn có chắc chắn muốn xóa nhân viên ${userDelete.username} (${userDelete.email}) ?`}
            </h6>
            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-t-10 m-r-10 m-l-10 m-b-10"
                onClick={() => deleteUser(userDelete.id)}
              >
                Submit
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-t-10 m-r-10 m-l-10 m-b-10"
                onClick={() => setUserDelete({})}
              >
                Cancel
              </Button>
            </div>
          </>
        </SweetAlert>
      )}
      <Table
        title="Danh sách nhân viên"
        showTitle
        table_th={users_th}
        table_tr={array}
        arr={_arr}
        minWidth={1300}
        perPage={_perPage}
        page={_page}
        totalItems={totalUsers}
        sortAsc={_sortAsc}
        keywordAllField={keywordAllField}
        loading={loading}
        variant="user"
        error={error}
        handlePerPage={handlePerPage}
        handleSort={handleSort}
        handleOnChange={handleOnChange}
        handleDelete={handleDelete}
        handleTableReset={handleTableReset}
      />
    </>
  );
};

export default ViewAllUsers;
