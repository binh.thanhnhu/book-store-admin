import { useAppDispatch, useAppSelector } from "app/hooks";
import Information from "components/Infomation/Information";
import { users_th_info } from "constants/data";
import { IUser } from "constants/interface";
import { getUserRequest } from "features/user/userSlice";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const UserInfomationPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const user = useAppSelector((state) => state.user.user);
  const dispatch = useAppDispatch();

  let temp: any = {};
  let userMap: any = {};
  users_th_info.forEach((item) => {
    userMap[item.key] = item;
    temp[item.key] = user && user[item.key as keyof IUser];
  });

  useEffect(() => {
    dispatch(getUserRequest(id));
  }, [dispatch, id]);

  return <Information item={temp} itemMap={userMap} />;
};

export default UserInfomationPage;
