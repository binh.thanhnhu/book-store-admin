import MenuItem from "@material-ui/core/MenuItem";
import { useAppDispatch, useAppSelector } from "app/hooks";
import Button from "components/Button/Button";
import Card from "components/Card/Card";
import TextField from "components/TextField/TextField";
import { OptionUserRole } from "constants/data";
import { VUserUpdate } from "constants/validation";
import { getUserRequest, updateUserRequest } from "features/user/userSlice";
import { FastField, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const UpdateUserPage: React.FC = () => {
  const user = useAppSelector((state) => state.user.user);
  const dispatch = useAppDispatch();
  const { id } = useParams<{ id: string }>();

  const handleSubmit = (values: any) => {
    delete values.username;
    dispatch(updateUserRequest({ ...values, id }));
  };

  useEffect(() => {
    dispatch(getUserRequest(id));
  }, [dispatch, id]);

  return (
    <Card>
      <Formik
        onSubmit={handleSubmit}
        initialValues={{
          username: user?.username,
          firstName: user?.firstName,
          lastName: user?.lastName,
          phone: user?.phone,
          country: user?.country,
          postcode: user?.postcode,
          address: user?.address,
          email: user?.email,
          role: user?.role,
        }}
        validationSchema={VUserUpdate}
        enableReinitialize
      >
        {(formik) => (
          <Form className="row">
            <div className="col-lg-6 p-t-24">
              <FastField
                disabled
                component={TextField}
                label="Tài khoản"
                name="username"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tên" name="firstName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Họ" name="lastName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Số điện thoại"
                name="phone"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Email" name="email" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Chức vụ"
                select
                name="role"
              >
                {OptionUserRole.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </FastField>
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Quốc gia"
                name="country"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Postcode"
                name="postcode"
              />
            </div>
            <div className="col-lg-12 p-t-24">
              <FastField
                component={TextField}
                label="Địa chỉ"
                name="address"
                multiline
                rowsMax={4}
              />
            </div>

            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              >
                Cập nhật
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
                onClick={() => formik.resetForm()}
              >
                Hủy bỏ
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Card>
  );
};

export default UpdateUserPage;
