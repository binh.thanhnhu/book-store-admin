import { useAppDispatch, useAppSelector } from "app/hooks";
import Information from "components/Infomation/Information";
import { customers_th_info } from "constants/data";
import { ICustomer } from "constants/interface";
import { getCustomerRequest } from "features/customer/customerSlice";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const CustomerInfomationPage: React.FC = () => {
  const { id } = useParams<{ id: string }>();
  const customer = useAppSelector((state) => state.customer.customer);
  const dispatch = useAppDispatch();

  let temp: any = {};
  let customerMap: any = {};
  customers_th_info.forEach((item) => {
    customerMap[item.key] = item;
    temp[item.key] = customer && customer[item.key as keyof ICustomer];
  });

  useEffect(() => {
    dispatch(getCustomerRequest(id));
  }, [dispatch, id]);

  return <Information item={temp} itemMap={customerMap} />;
};

export default CustomerInfomationPage;
