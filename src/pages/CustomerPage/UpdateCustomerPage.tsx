import { useAppDispatch, useAppSelector } from "app/hooks";
import Button from "components/Button/Button";
import Card from "components/Card/Card";
import TextField from "components/TextField/TextField";
import { VCustomerUpdate } from "constants/validation";
import {
  getCustomerRequest,
  updateCustomerRequest,
} from "features/customer/customerSlice";
import { FastField, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

const UpdateCustomerPage: React.FC = () => {
  const customer = useAppSelector((state) => state.customer.customer);
  const dispatch = useAppDispatch();
  const { id } = useParams<{ id: string }>();

  const handleSubmit = (values: any) => {
    delete values.username;
    dispatch(updateCustomerRequest({ ...values, id }));
  };

  useEffect(() => {
    dispatch(getCustomerRequest(id));
  }, [dispatch, id]);

  return (
    <Card>
      <Formik
        onSubmit={handleSubmit}
        initialValues={{
          username: customer?.username,
          firstName: customer?.firstName,
          lastName: customer?.lastName,
          phone: customer?.phone,
          company: customer?.company,
          country: customer?.country,
          postcode: customer?.postcode,
          address: customer?.address,
          email: customer?.email,
        }}
        validationSchema={VCustomerUpdate}
        enableReinitialize
      >
        {(formik) => (
          <Form className="row">
            <div className="col-lg-6 p-t-24">
              <FastField
                disabled
                component={TextField}
                label="Tài khoản"
                name="username"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Tên" name="firstName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Họ" name="lastName" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Số điện thoại"
                name="phone"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Email" name="email" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField component={TextField} label="Công ty" name="company" />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Quốc gia"
                name="country"
              />
            </div>
            <div className="col-lg-6 p-t-24">
              <FastField
                component={TextField}
                label="Postcode"
                name="postcode"
              />
            </div>
            <div className="col-lg-12 p-t-24">
              <FastField
                component={TextField}
                label="Địa chỉ"
                name="address"
                multiline
                rowsMax={4}
              />
            </div>

            <div className="col-lg-12 p-t-24 text-center m-t-10">
              <Button
                type="submit"
                variant="contained"
                className="btn-pink mdl-button m-r-10 m-l-10 m-b-10"
              >
                Cập nhật
              </Button>
              <Button
                variant="contained"
                className="btn-default mdl-button m-r-10 m-l-10 m-b-10"
                onClick={() => formik.resetForm()}
              >
                Hủy bỏ
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Card>
  );
};

export default UpdateCustomerPage;
