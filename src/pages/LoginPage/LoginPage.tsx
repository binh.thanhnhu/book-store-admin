import { useAppDispatch } from "app/hooks";
import classNames from "classnames";
import { loginRequest } from "features/auth/authSlice";
import React from "react";
import { useState } from "react";

import s from "./LoginPage.module.css";

const LoginPage = () => {
  const [show, setShow] = useState<boolean>(false);
  const [check, setCheck] = useState<boolean>(true);
  const dispatch = useAppDispatch();

  const handleSubmit = (event: any) => {
    event.preventDefault();
    dispatch(
      loginRequest({
        username: event.target.username.value,
        password: event.target.password.value,
      })
    );
  };

  const handleInvalid = (event: any) => {
    event.target.setCustomValidity("Vui lòng nhập đủ thông tin");
  };

  const handleValid = (event: any) => {
    event.target.setCustomValidity("");
  };

  const handlePasswordShow = (): void => {
    setShow(!show);
  };

  const handleRemember = (): void => {
    setCheck(!check);
  };

  return (
    <div className={s.root}>
      <section className={s.section}>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-6 text-center mb-5">
              <h2 className={s.headingSection}>Đăng nhập</h2>
            </div>
          </div>
          <div className="row justify-content-center">
            <div className="col-md-6 col-lg-4">
              <div className={classNames(s.loginWrap, "p-0")}>
                <form className="signin-form" onSubmit={handleSubmit}>
                  <div className={s.formGroup}>
                    <input
                      type="text"
                      className={classNames(s.formControl, "form-control")}
                      placeholder="Tài khoản"
                      required
                      onInvalid={(e) => handleInvalid(e)}
                      onInput={(e) => handleValid(e)}
                      name="username"
                    />
                  </div>
                  <div className={s.formGroup}>
                    <input
                      id="password-field"
                      type={show ? "text" : "password"}
                      className={classNames(s.formControl, "form-control")}
                      placeholder="Mật khẩu"
                      required
                      onInvalid={(e) => handleInvalid(e)}
                      onInput={(e) => handleValid(e)}
                      name="password"
                    />
                    <span
                      className={classNames(s.fieldIcon, "material-icons")}
                      onClick={() => handlePasswordShow()}
                    >
                      {show ? "visibility_off" : "visibility"}
                    </span>
                  </div>
                  <div className={s.formGroup}>
                    <button
                      type="submit"
                      className={classNames(
                        s.formControl,
                        "form-control btn btn-primary submit px-3"
                      )}
                    >
                      Đăng Nhập
                    </button>
                  </div>
                  <p className="w-100 m-t-10 text-center">
                    Staff Account For Demo: test10 / 123456
                  </p>
                  <div className={classNames(s.formGroup, "d-md-flex")}>
                    <div className="w-50">
                      <label
                        className={classNames(
                          s.checkboxWrap,
                          s.checkboxPrimary
                        )}
                      >
                        Remember Me
                        <input type="checkbox" checked={check} hidden />
                        <span
                          onClick={() => handleRemember()}
                          className={classNames(s.checkmark, "material-icons")}
                        >
                          {check ? "check_box" : "check_box_outline_blank"}
                        </span>
                      </label>
                    </div>
                    <div className="w-50 text-md-right">
                      <a href="/#" style={{ color: "#fff" }}>
                        Quên Mật Khẩu
                      </a>
                    </div>
                  </div>
                </form>
                <p className="w-100 text-center">— Hoặc Đăng Nhập Với —</p>
                <div className={classNames(s.social, "d-flex text-center")}>
                  <a href="/#" className="px-2 py-2 mr-md-1 rounded">
                    <span className="ion-logo-facebook mr-2" /> Facebook
                  </a>
                  <a href="/#" className="px-2 py-2 ml-md-1 rounded">
                    <span className="ion-logo-twitter mr-2" /> Twitter
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default LoginPage;
