import { useAppDispatch, useAppSelector } from "app/hooks";
import SaleAndOrder from "components/Charts/SaleAndOrder";
import TypeProduct from "components/Charts/TypeProduct";
import StateOverview from "components/StateOverview/StateOverview";
import { IStatistics, ITotal } from "constants/interface";
import { getStatisticsRequest } from "features/statistics/statisticsSlice";
import ViewAllOrdersPage from "pages/OrderPage/ViewAllOrdersPage";
import React, { useEffect, useState } from "react";
import s from "./HomePage.module.css";

const TOTAL: ITotal[] = [
  {
    value: "totalSalesMoney",
    title: "Sales",
    number: 0,
    icon: "style",
    percent: 60,
    background: "bg-blue",
    money: true,
  },
  {
    value: "totalCustomers",
    title: "Customers",
    number: 0,
    icon: "card_travel",
    percent: 40,
    background: "bg-orange",
  },
  // {
  //   value: "totalOrders",
  //   title: "Orders",
  //   number: 0,
  //   icon: "phone_in_talk",
  //   percent: 80,
  //   background: "bg-purple",
  // },
  {
    value: "totalVisitsSite",
    title: "Site visit",
    number: 0,
    icon: "monetization_on",
    percent: 60,
    background: "bg-success",
  },
];

const HomePage: React.FC = () => {
  const [total, setTotal] = useState<any>([]);
  const statistics = useAppSelector((state) => state.statistics.statistics);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getStatisticsRequest());
  }, [dispatch]);

  useEffect(() => {
    const temp = TOTAL.map((el) => {
      el.number = statistics[el.value as keyof IStatistics];
      return el;
    });
    setTotal(temp);
  }, [statistics]);

  useEffect(() => {}, []);

  return (
    <div className={s.stateOverview}>
      <div className="row">
        {total.map((o: ITotal, i: number) => (
          <StateOverview
            key={i}
            title={o.title}
            number={o.number}
            icon={o.icon}
            percent={o.percent}
            background={o.background}
            money={o.money}
          />
        ))}
      </div>
      <div className="row">
        <div className="col-sm-12 col-lg-8">
          <SaleAndOrder />
        </div>
        <div className="col-sm-12 col-lg-4">
          <TypeProduct />
        </div>
      </div>
      <ViewAllOrdersPage height={400} collapsible />
    </div>
  );
};

export default HomePage;
