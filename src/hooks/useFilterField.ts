import { useAppSelector } from "app/hooks";
import {
  LabelStatus,
  OptionForeignBook,
  OptionVietNameseBook,
  TypeProduct,
} from "constants/data";
import { ICustomer, IOrder, IProduct, IUser } from "constants/interface";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import calcItems from "utils/calcItems";
import formatPrice from "utils/formatPrice";
import getParams from "utils/getParams";

export default function useFilterField(
  items: any,
  totalItems: number,
  th: any,
  variant: "product" | "customer" | "user" | "order"
) {
  const [array, setArray] = useState<any>([]);
  const user = useAppSelector((state) => state.auth.user);
  const location = useLocation();

  useEffect(() => {
    let { page, perPage, sort } = getParams(location.search);
    sort = sort || "1";
    let { min, max } = calcItems(
      totalItems,
      perPage || "12",
      page || "1",
      sort === "1" ? "asc" : "desc"
    );

    const tempArr = items.map((item: IProduct & ICustomer & IOrder & IUser) => {
      let temp: any = {};

      if (variant === "product") {
        let typeProductMap: any = {};
        let optionVietNameseBookMap: any = {};
        let optionForeignBookMap: any = {};
        TypeProduct.forEach((el) => (typeProductMap[el.value] = el));
        OptionVietNameseBook.forEach(
          (el) => (optionVietNameseBookMap[el.value] = el)
        );
        OptionForeignBook.forEach(
          (el) => (optionForeignBookMap[el.value] = el)
        );

        th.map((obj: any) => {
          switch (obj.key) {
            case "no":
              if (sort === "1") temp["no"] = min;
              else temp["no"] = max;
              break;
            case "type":
              temp["type"] = typeProductMap[item.type as keyof IProduct]?.label;
              break;
            case "option":
              temp["option"] = (
                item.type === "VietNameseBook"
                  ? optionVietNameseBookMap
                  : optionForeignBookMap
              )[item.option as keyof IProduct]?.label;
              break;
            case "price":
              temp[obj.key] =
                formatPrice(item[obj.key as keyof IProduct] as number) + " đ";
              break;
            case "oldPrice":
              if (!item[obj.key as keyof IProduct]) temp[obj.key] = null;
              else
                temp[obj.key] =
                  formatPrice(item[obj.key as keyof IProduct] as number) + " đ";
              break;
            case "actions,info,edit,delete":
              user?._id === item.creatorId || user?.role === "admin"
                ? (temp["actions,info,edit,delete"] =
                    item[obj.key as keyof IProduct])
                : (temp["actions,info"] = item[obj.key as keyof IProduct]);
              break;
            default:
              temp[obj.key] = item[obj.key as keyof IProduct];
          }
          return null;
        });
      } else if (variant === "customer") {
        th.map((obj: any) => {
          switch (obj.key) {
            case "no":
              if (sort === "1") temp["no"] = min;
              else temp["no"] = max;
              break;
            case "actions,info,edit,delete":
              user?.role === "admin"
                ? (temp["actions,info,edit,delete"] =
                    item[obj.key as keyof ICustomer])
                : (temp["actions,info"] = item[obj.key as keyof ICustomer]);
              break;
            default:
              temp[obj.key] = item[obj.key as keyof ICustomer];
          }
          return null;
        });
      } else if (variant === "user") {
        th.map((obj: any) => {
          switch (obj.key) {
            case "no":
              if (sort === "1") temp["no"] = min;
              else temp["no"] = max;
              break;
            case "actions,info,edit,delete":
              user?.role === "admin"
                ? (temp["actions,info,edit,delete"] =
                    item[obj.key as keyof IUser])
                : (temp["actions,info"] = item[obj.key as keyof IUser]);
              break;
            default:
              temp[obj.key] = item[obj.key as keyof IUser];
          }
          return null;
        });
      } else if (variant === "order") {
        let statusMap: any = {};
        LabelStatus.forEach((el) => (statusMap[el.value] = el));

        th.map((obj: any) => {
          switch (obj.key) {
            case "no":
              if (sort === "1") temp["no"] = min;
              else temp["no"] = max;
              break;
            case "totalPrice":
              temp["totalPrice"] = formatPrice(item.cart.totalPrice) + " đ";
              break;
            case "status":
              temp["status"] = statusMap[item.status as keyof IOrder];
              break;
            case "actions,edit,delete":
              user?.role === "admin"
                ? (temp["actions,edit,delete"] = item[obj.key as keyof IOrder])
                : (temp["actions,edit"] = item[obj.key as keyof IOrder]);
              break;

            default:
              temp[obj.key] = item[obj.key as keyof IOrder];
          }
          return null;
        });
      }

      sort === "1" ? min <= max && min++ : max >= min && max--;

      return temp;
    });

    setArray(tempArr);
  }, [location, items, totalItems, th, variant, user?.role, user?._id]);

  return { array };
}
