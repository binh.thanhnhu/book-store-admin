import { useEffect, useState } from "react";

export default function useSetHeightTable(ref: any, subtractHeight: number) {
  const [maxHeight, setMaxHeight] = useState<string>("unset");

  useEffect(() => {
    setMaxHeight(
      `calc(100vh - ${
        ref.current?.getBoundingClientRect().top
      }px - ${subtractHeight}px)`
    );
  }, [ref, subtractHeight]);

  return { maxHeight };
}
