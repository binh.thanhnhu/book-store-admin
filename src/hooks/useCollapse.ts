import { useEffect, useState } from "react";

export default function useCollapse(
  ref: any,
  heightLimit: number,
  select?: boolean,
  type?: string
) {
  const [collapse, setCollapse] = useState<boolean>(false);
  const [height, setHeight] = useState<number | undefined>(0);

  useEffect(() => {
    if (type === "sidebar") {
      select ? setHeight(ref?.current?.scrollHeight) : setHeight(heightLimit);
    } else {
      collapse ? setHeight(heightLimit) : setHeight(ref?.current?.scrollHeight);
    }
  }, [ref, collapse, heightLimit, select, type]);

  return { collapse, setCollapse, height };
}
