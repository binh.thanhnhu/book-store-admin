import { useEffect, useState } from "react";

export default function useCountUp(end: number): number {
  const [endState, setEndState] = useState<number>(0);

  useEffect(() => {
    let timer: any;
    if (endState < end) {
      timer = setInterval(() => {
        setEndState(endState + 5);
      }, 100);
    }
    return () => {
      clearInterval(timer);
    };
  }, [end, endState]);

  return endState;
}
