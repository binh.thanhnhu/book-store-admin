import {
  configureStore,
  combineReducers,
  ThunkAction,
  Action,
} from "@reduxjs/toolkit";
import productReducer from "features/product/productSlice";
import createSagaMiddleware from "@redux-saga/core";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";
import rootSaga from "./rootSaga";
import globalReducer from "features/global/globalSlice";
import customerReducer from "features/customer/customerSlice";
import userReducer from "features/user/userSlice";
import orderReducer from "features/order/orderSlice";
import authReducer from "features/auth/authSlice";
import statisticsReducer from "features/statistics/statisticsSlice";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history),
  product: productReducer,
  customer: customerReducer,
  global: globalReducer,
  user: userReducer,
  order: orderReducer,
  auth: authReducer,
  statistics: statisticsReducer,
});

const sagaMiddleware = createSagaMiddleware();
export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ thunk: false }).concat(
      sagaMiddleware,
      routerMiddleware(history)
    ),
  devTools: process.env.NODE_ENV === "development",
});

sagaMiddleware.run(rootSaga);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
