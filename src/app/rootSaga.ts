import { fork } from "redux-saga/effects";
import productSaga from "features/product/productSaga";
import customerSaga from "features/customer/customerSaga";
import userSaga from "features/user/userSaga";
import orderSaga from "features/order/orderSaga";
import authSage from "features/auth/authSage";
import statisticsSaga from "features/statistics/statisticsSaga";
import globalSaga from "features/global/globalSaga";

export default function* rootSaga(): any {
  yield fork(productSaga);
  yield fork(customerSaga);
  yield fork(userSaga);
  yield fork(orderSaga);
  yield fork(authSage);
  yield fork(statisticsSaga);
  yield fork(globalSaga);
}
