import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICustomer, IParam, IResponse } from "constants/interface";

interface ICustomerState {
  loading: boolean;
  message: {
    getAllCustomersByOption: string;
    deleteCustomer: string;
    createNewCustomer: string;
    updateCustomer: string;
  };
  error: {
    getAllCustomersByOption: string;
    deleteCustomer: string;
    createNewCustomer: string;
    updateCustomer: string;
    getCustomer: string;
  };
  customers: ICustomer[];
  customer: ICustomer | null;
  totalPages: number;
  totalCustomers: number;
}

const initialState: ICustomerState = {
  loading: false,
  message: {
    getAllCustomersByOption: "",
    deleteCustomer: "",
    createNewCustomer: "",
    updateCustomer: "",
  },
  error: {
    getAllCustomersByOption: "",
    deleteCustomer: "",
    createNewCustomer: "",
    updateCustomer: "",
    getCustomer: "",
  },
  customers: [],
  customer: null,
  totalPages: 0,
  totalCustomers: 0,
};

const customerSlice = createSlice({
  name: "customer",
  initialState,
  reducers: {
    getAllCustomersByOptionRequest: (
      state,
      { payload }: PayloadAction<IParam>
    ) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getAllCustomersByOptionSuccess: (
      state,
      { payload }: PayloadAction<any>
    ) => {
      state.customers = payload.category;
      state.totalPages = payload.totalPages || state.totalPages;
      state.totalCustomers = payload.totalCustomers || state.totalCustomers;
      state.error.getAllCustomersByOption = "";
      state.loading = false;
    },
    getAllCustomersByOptionFailure: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.loading = false;
      state.error.getAllCustomersByOption = payload.message;
    },
    createNewCustomerRequest: (
      state,
      { payload }: PayloadAction<ICustomer>
    ) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    createNewCustomerSuccess: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.error.createNewCustomer = "";
      state.message.createNewCustomer = payload.message;
      state.loading = false;
    },
    createNewCustomerFailure: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.message.createNewCustomer = "";
      state.error.createNewCustomer = payload.message;
      state.loading = false;
    },
    getCustomerRequest: (state, { payload }: PayloadAction<string>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getCustomerSuccess: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.customer = payload.data;
      state.loading = false;
    },
    getCustomerFailure: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.customer = null;
      state.error.getCustomer = payload.message;
      state.loading = false;
    },
    updateCustomerRequest: (state, { payload }: PayloadAction<ICustomer>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    updateCustomerSuccess: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.error.updateCustomer = "";
      state.message.updateCustomer = payload.message;
      state.loading = false;
    },
    updateCustomerFailure: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.message.updateCustomer = "";
      state.error.updateCustomer = payload.message;
      state.loading = false;
    },
    deleteCustomerRequest: (state, { payload }: PayloadAction<string>) => {
      state.message = initialState.message;
      state.error = initialState.error;
    },
    deleteCustomerSuccess: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.message.deleteCustomer = payload.message;
      state.error.deleteCustomer = "";
    },
    deleteCustomerFailure: (
      state,
      { payload }: PayloadAction<IResponse<ICustomer>>
    ) => {
      state.error.deleteCustomer = payload.message;
      state.message.deleteCustomer = "";
    },
  },
});

export const {
  getAllCustomersByOptionRequest,
  getAllCustomersByOptionSuccess,
  getAllCustomersByOptionFailure,
  createNewCustomerRequest,
  createNewCustomerSuccess,
  createNewCustomerFailure,
  getCustomerRequest,
  getCustomerSuccess,
  getCustomerFailure,
  updateCustomerRequest,
  updateCustomerSuccess,
  updateCustomerFailure,
  deleteCustomerRequest,
  deleteCustomerSuccess,
  deleteCustomerFailure,
} = customerSlice.actions;
export default customerSlice.reducer;
