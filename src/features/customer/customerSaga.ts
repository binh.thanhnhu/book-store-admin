import { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { ROUTES } from "constants/data";
import { ICategory, ICustomer, IParam, IResponse } from "constants/interface";
import {
  addNotificationRequest,
  startLoadingGlobal,
  stopLoadingGlobal,
} from "features/global/globalSlice";
import moment from "moment";
import { toast } from "react-toastify";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  createNewCustomerAPI,
  deleteCustomerAPI,
  getAllCustomersByOptionAPI,
  getCustomerAPI,
  updateCustomerAPI,
} from "./customerAPI";
import {
  createNewCustomerFailure,
  createNewCustomerRequest,
  createNewCustomerSuccess,
  deleteCustomerFailure,
  deleteCustomerRequest,
  deleteCustomerSuccess,
  getAllCustomersByOptionFailure,
  getAllCustomersByOptionRequest,
  getAllCustomersByOptionSuccess,
  getCustomerFailure,
  getCustomerRequest,
  getCustomerSuccess,
  updateCustomerFailure,
  updateCustomerRequest,
  updateCustomerSuccess,
} from "./customerSlice";

function* handleGetAllCustomersByOption({ payload }: PayloadAction<IParam>) {
  try {
    const temp = { ...payload, keywordAllFieldFix: "" };
    const res: AxiosResponse<IResponse<ICategory>> = yield call(
      getAllCustomersByOptionAPI,
      temp
    );
    yield put(getAllCustomersByOptionSuccess(res.data.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getAllCustomersByOptionFailure(error.response.data));
    yield put(stopLoadingGlobal());
  }
}

function* handleCreateNewCustomer({ payload }: PayloadAction<ICustomer>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<ICustomer>> = yield call(
      createNewCustomerAPI,
      payload,
      token
    );
    yield put(createNewCustomerSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(createNewCustomerFailure(error.response.data));
    yield put(stopLoadingGlobal());
    addNotificationRequest({
      option: "error",
      message: error.response.data.message,
      time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
      toast: () => toast.error(error.response.data.message),
    });
  }
}

function* handleGetCustomer({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<ICustomer>> = yield call(
      getCustomerAPI,
      payload
    );
    yield put(getCustomerSuccess(res.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getCustomerFailure(error.response.data));
    yield put(stopLoadingGlobal());
    addNotificationRequest({
      option: "error",
      message: error.response.data.message,
      time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
      toast: () => toast.error(error.response.data.message),
    });
  }
}

function* handleUpdateCustomer({
  payload: { id, ...values },
}: PayloadAction<any>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<ICustomer>> = yield call(
      updateCustomerAPI,
      id,
      values,
      token
    );
    yield put(updateCustomerSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.customer.data.update?.path}/${id}`,
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(updateCustomerFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.customer.data.update?.path}/${id}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleDeleteCustomer({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<ICustomer>> = yield call(
      deleteCustomerAPI,
      payload,
      token
    );
    yield put(stopLoadingGlobal());
    const { params } = yield select((state: RootState) => state.customer);
    yield put(getAllCustomersByOptionRequest(params));
    yield put(deleteCustomerSuccess(res.data));
    addNotificationRequest({
      option: "success",
      message: res.data.message,
      time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
      toast: () => toast.success(res.data.message),
    });
  } catch (error) {
    yield put(deleteCustomerFailure(error.response.data));
    yield put(stopLoadingGlobal());
    addNotificationRequest({
      option: "error",
      message: error.response.data.message,
      time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
      path: `${ROUTES.customer.data.list?.path}`,
      toast: () => toast.error(error.response.data.message),
    });
  }
}

export default function* customerSaga() {
  yield takeLatest(
    getAllCustomersByOptionRequest.type,
    handleGetAllCustomersByOption
  );
  yield takeLatest(createNewCustomerRequest.type, handleCreateNewCustomer);
  yield takeLatest(getCustomerRequest.type, handleGetCustomer);
  yield takeLatest(updateCustomerRequest.type, handleUpdateCustomer);
  yield takeLatest(deleteCustomerRequest.type, handleDeleteCustomer);
}
