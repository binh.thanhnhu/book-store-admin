// import { AxiosResponse } from "axios";
import { ICustomer, IParam } from "constants/interface";
import addParams from "utils/addParams";
import callApi from "utils/apiCaller";

export const getAllCustomersByOptionAPI = (params: IParam) => {
  const url = "api/customer/option?" + addParams("", params);
  return callApi("GET", url);
};

export const createNewCustomerAPI = (values: ICustomer, token: string) => {
  return callApi("POST", "api/customer/register", values, token);
};

export const getCustomerAPI = (id: string) => {
  return callApi("GET", `api/customer/get-by-id/${id}`);
};

export const updateCustomerAPI = (
  id: string,
  values: ICustomer,
  token: string
) => {
  return callApi("PUT", `api/customer/update/${id}`, values, token);
};

export const deleteCustomerAPI = (id: string, token: string) => {
  return callApi("DELETE", `api/customer/delete/${id}`, null, token);
};
