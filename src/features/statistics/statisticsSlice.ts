import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IResponse, IStatistics } from "constants/interface";

interface IStatisticsState {
  loading: boolean;
  statistics: IStatistics;
  salesOfWeek: number[];
  ordersOfWeek: number[];
  error: {
    statistics: string;
    salesOfWeek: string;
    ordersOfWeek: string;
  };
}

const initialState: IStatisticsState = {
  loading: false,
  statistics: {
    totalSales: 0,
    totalSalesMoney: 0,
    totalCustomers: 0,
    totalOrders: 0,
    totalVisitsSite: 0,
    totalVietnameseBooks: 0,
    totalForeignBooks: 0,
    totalStationeries: 0,
    totalSouvenirs: 0,
    totalEbooks: 0,
  },
  salesOfWeek: [],
  ordersOfWeek: [],
  error: {
    statistics: "",
    salesOfWeek: "",
    ordersOfWeek: "",
  },
};

const statisticsSlice = createSlice({
  name: "statistics",
  initialState,
  reducers: {
    getStatisticsRequest: (state) => {
      state.loading = true;
      state.error = initialState.error;
    },
    getStatisticsSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IStatistics>>
    ) => {
      state.statistics = payload.data;
    },
    getStatisticsFailure: (
      state,
      { payload }: PayloadAction<IResponse<string>>
    ) => {
      state.error.statistics = payload.message;
    },
    getSalesOfWeekRequest: (state) => {
      state.loading = true;
      state.error = initialState.error;
    },
    getSalesOfWeekSuccess: (
      state,
      { payload }: PayloadAction<IResponse<number[]>>
    ) => {
      state.salesOfWeek = payload.data;
    },
    getSalesOfWeekFailure: (
      state,
      { payload }: PayloadAction<IResponse<string>>
    ) => {
      state.error.salesOfWeek = payload.message;
    },
    getOrdersOfWeekRequest: (state) => {
      state.loading = true;
      state.error = initialState.error;
    },
    getOrdersOfWeekSuccess: (
      state,
      { payload }: PayloadAction<IResponse<number[]>>
    ) => {
      state.ordersOfWeek = payload.data;
    },
    getOrdersOfWeekFailure: (
      state,
      { payload }: PayloadAction<IResponse<string>>
    ) => {
      state.error.ordersOfWeek = payload.message;
    },
  },
});

export const {
  getStatisticsRequest,
  getStatisticsSuccess,
  getStatisticsFailure,
  getSalesOfWeekRequest,
  getSalesOfWeekSuccess,
  getSalesOfWeekFailure,
  getOrdersOfWeekRequest,
  getOrdersOfWeekSuccess,
  getOrdersOfWeekFailure,
} = statisticsSlice.actions;
export default statisticsSlice.reducer;
