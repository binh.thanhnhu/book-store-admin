import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { IResponse, IStatistics } from "constants/interface";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  getOrdersOfWeekAPI,
  getSalesOfWeekAPI,
  getStatisticsAPI,
} from "./statisticsAPI";
import {
  getOrdersOfWeekFailure,
  getOrdersOfWeekRequest,
  getOrdersOfWeekSuccess,
  getSalesOfWeekFailure,
  getSalesOfWeekRequest,
  getSalesOfWeekSuccess,
  getStatisticsFailure,
  getStatisticsRequest,
  getStatisticsSuccess,
} from "./statisticsSlice";

function* handleGetStatistics() {
  try {
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IStatistics>> = yield call(
      getStatisticsAPI,
      token
    );
    yield put(getStatisticsSuccess(res.data));
  } catch (error) {
    yield put(getStatisticsFailure(error.response.data));
  }
}

function* handleGetSalesOfWeek() {
  try {
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<number[]>> = yield call(
      getSalesOfWeekAPI,
      token
    );
    yield put(getSalesOfWeekSuccess(res.data));
  } catch (error) {
    yield put(getSalesOfWeekFailure(error.response.data));
  }
}

function* handleGetOrdersOfWeek() {
  try {
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<number[]>> = yield call(
      getOrdersOfWeekAPI,
      token
    );
    yield put(getOrdersOfWeekSuccess(res.data));
  } catch (error) {
    yield put(getOrdersOfWeekFailure(error.response.data));
  }
}

export default function* userSaga() {
  yield takeLatest(getStatisticsRequest.type, handleGetStatistics);
  yield takeLatest(getSalesOfWeekRequest.type, handleGetSalesOfWeek);
  yield takeLatest(getOrdersOfWeekRequest.type, handleGetOrdersOfWeek);
}
