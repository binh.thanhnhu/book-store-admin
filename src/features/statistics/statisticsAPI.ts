import callApi from "utils/apiCaller";

export const getStatisticsAPI = (token: string) => {
  return callApi("GET", "api/statistics/getAll", null, token);
};

export const increaseSiteVisitAPI = () => {
  return callApi("POST", "api/statistics/increaseSiteVisit", null);
};

export const getSalesOfWeekAPI = (token: string) => {
  return callApi("GET", "api/statistics/getSalesOfWeek", null, token);
};

export const getOrdersOfWeekAPI = (token: string) => {
  return callApi("GET", "api/statistics/getOrdersOfWeek", null, token);
};
