import { ILogin } from "constants/interface";
import callApi from "utils/apiCaller";

export const loginAPI = (values: ILogin) => {
  return callApi("POST", "api/user/login", values);
};
