import { PayloadAction } from "@reduxjs/toolkit";
import { AxiosResponse } from "axios";
import { push } from "connected-react-router";
import { ILogin, IResponseLogin, IUser } from "constants/interface";
import {
  loginFailure,
  loginRequest,
  loginSuccess,
} from "features/auth/authSlice";
import {
  startLoadingGlobal,
  stopLoadingGlobal,
} from "features/global/globalSlice";
import { toast } from "react-toastify";
import { call, put, takeLatest } from "redux-saga/effects";
import { loginAPI } from "./authAPI";

function* handleLogin({ payload }: PayloadAction<ILogin>) {
  try {
    yield put(startLoadingGlobal());
    const res: AxiosResponse<IResponseLogin<IUser>> = yield call(
      loginAPI,
      payload
    );
    yield put(loginSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(push("/app/dashboard"));
  } catch (error) {
    yield put(loginFailure(error.response.data));
    yield put(stopLoadingGlobal());
    toast.error(error.response.data.message);
  }
}

export default function* userSaga() {
  yield takeLatest(loginRequest.type, handleLogin);
}
