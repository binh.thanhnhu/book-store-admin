import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ILogin, IResponseLogin, IUser } from "constants/interface";

interface IAuthState {
  user: IUser | null;
  token: string;
  error: {
    login: string;
  };
}

const initialState: IAuthState = {
  token: localStorage.getItem("user_token") || "",
  user: JSON.parse(localStorage.getItem("user_info") || "{}") || null,
  error: {
    login: "",
  },
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loginRequest: (state, { payload }: PayloadAction<ILogin>) => {},
    loginSuccess: (
      state,
      { payload }: PayloadAction<IResponseLogin<IUser>>
    ) => {
      state.error.login = "";
      localStorage.setItem("user_token", payload.token);
      localStorage.setItem("user_info", JSON.stringify(payload.data));
      state.token = payload.token || "";
      state.user = payload.data;
    },
    loginFailure: (
      state,
      { payload }: PayloadAction<IResponseLogin<IUser>>
    ) => {
      state.error.login = payload.message;
    },
    logout: (state) => {
      state.token = "";
      localStorage.removeItem("user_token");
    },
  },
});

export const { loginRequest, loginSuccess, loginFailure, logout } =
  authSlice.actions;
export default authSlice.reducer;
