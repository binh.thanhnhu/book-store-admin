import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IParam, IResponse, IUser } from "constants/interface";

interface IUserState {
  loading: boolean;
  message: {
    getAllUsersByOption: string;
    deleteUser: string;
    createNewUser: string;
    updateUser: string;
  };
  error: {
    getAllUsersByOption: string;
    deleteUser: string;
    createNewUser: string;
    updateUser: string;
    getUser: string;
    login: string;
  };
  users: IUser[];
  user: IUser | null;
  totalPages: number;
  totalUsers: number;
}

const initialState: IUserState = {
  loading: false,
  message: {
    getAllUsersByOption: "",
    deleteUser: "",
    createNewUser: "",
    updateUser: "",
  },
  error: {
    getAllUsersByOption: "",
    deleteUser: "",
    createNewUser: "",
    updateUser: "",
    getUser: "",
    login: "",
  },
  users: [],
  user: null,
  totalPages: 0,
  totalUsers: 0,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    getAllUsersByOptionRequest: (state, { payload }: PayloadAction<IParam>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getAllUsersByOptionSuccess: (state, { payload }: PayloadAction<any>) => {
      state.users = payload.category;
      state.totalPages = payload.totalPages || state.totalPages;
      state.totalUsers = payload.totalUsers || state.totalUsers;
      state.error.getAllUsersByOption = "";
      state.loading = false;
    },
    getAllUsersByOptionFailure: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.loading = false;
      state.error.getAllUsersByOption = payload.message;
    },
    createNewUserRequest: (state, { payload }: PayloadAction<IUser>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    createNewUserSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.error.createNewUser = "";
      state.message.createNewUser = payload.message;
      state.loading = false;
    },
    createNewUserFailure: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.message.createNewUser = "";
      state.error.createNewUser = payload.message;
      state.loading = false;
    },
    getUserRequest: (state, { payload }: PayloadAction<string>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getUserSuccess: (state, { payload }: PayloadAction<IResponse<IUser>>) => {
      state.user = payload.data;
      state.loading = false;
    },
    getUserFailure: (state, { payload }: PayloadAction<IResponse<IUser>>) => {
      state.user = null;
      state.error.getUser = payload.message;
      state.loading = false;
    },
    updateUserRequest: (state, { payload }: PayloadAction<IUser>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    updateUserSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.error.updateUser = "";
      state.message.updateUser = payload.message;
      state.loading = false;
    },
    updateUserFailure: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.message.updateUser = "";
      state.error.updateUser = payload.message;
      state.loading = false;
    },
    deleteUserRequest: (state, { payload }: PayloadAction<string>) => {
      state.message = initialState.message;
      state.error = initialState.error;
    },
    deleteUserSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.message.deleteUser = payload.message;
      state.error.deleteUser = "";
    },
    deleteUserFailure: (
      state,
      { payload }: PayloadAction<IResponse<IUser>>
    ) => {
      state.error.deleteUser = payload.message;
      state.message.deleteUser = "";
    },
  },
});

export const {
  getAllUsersByOptionRequest,
  getAllUsersByOptionSuccess,
  getAllUsersByOptionFailure,
  createNewUserRequest,
  createNewUserSuccess,
  createNewUserFailure,
  getUserRequest,
  getUserSuccess,
  getUserFailure,
  updateUserRequest,
  updateUserSuccess,
  updateUserFailure,
  deleteUserRequest,
  deleteUserSuccess,
  deleteUserFailure,
} = userSlice.actions;
export default userSlice.reducer;
