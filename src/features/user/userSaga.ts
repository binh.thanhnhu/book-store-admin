import { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { ROUTES } from "constants/data";
import { ICategory, IParam, IResponse, IUser } from "constants/interface";
import {
  addNotificationRequest,
  startLoadingGlobal,
  stopLoadingGlobal,
} from "features/global/globalSlice";
import moment from "moment";
import { toast } from "react-toastify";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  createNewUserAPI,
  deleteUserAPI,
  getAllUsersByOptionAPI,
  getUserAPI,
  updateUserAPI,
} from "./userAPI";
import {
  createNewUserFailure,
  createNewUserRequest,
  createNewUserSuccess,
  deleteUserFailure,
  deleteUserRequest,
  deleteUserSuccess,
  getAllUsersByOptionFailure,
  getAllUsersByOptionRequest,
  getAllUsersByOptionSuccess,
  getUserFailure,
  getUserRequest,
  getUserSuccess,
  updateUserFailure,
  updateUserRequest,
  updateUserSuccess,
} from "./userSlice";

function* handleGetAllUsersByOption({ payload }: PayloadAction<IParam>) {
  try {
    const temp = { ...payload, keywordAllFieldFix: "" };
    const res: AxiosResponse<IResponse<ICategory>> = yield call(
      getAllUsersByOptionAPI,
      temp
    );
    yield put(getAllUsersByOptionSuccess(res.data.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getAllUsersByOptionFailure(error.response.data));
    yield put(stopLoadingGlobal());
  }
}

function* handleCreateNewUser({ payload }: PayloadAction<IUser>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IUser>> = yield call(
      createNewUserAPI,
      payload,
      token
    );
    yield put(createNewUserSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(createNewUserFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleGetUser({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IUser>> = yield call(
      getUserAPI,
      payload
    );
    yield put(getUserSuccess(res.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getUserFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleUpdateUser({ payload: { id, ...values } }: PayloadAction<any>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IUser>> = yield call(
      updateUserAPI,
      id,
      values,
      token
    );
    yield put(updateUserSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.user.data.update?.path}/${id}`,
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(updateUserFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.user.data.update?.path}/${id}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleDeleteUser({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IUser>> = yield call(
      deleteUserAPI,
      payload,
      token
    );
    yield put(stopLoadingGlobal());
    const { params } = yield select((state: RootState) => state.user);
    yield put(getAllUsersByOptionRequest(params));
    yield put(deleteUserSuccess(res.data));
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(deleteUserFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.user.data.list?.path}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

export default function* userSaga() {
  yield takeLatest(getAllUsersByOptionRequest.type, handleGetAllUsersByOption);
  yield takeLatest(createNewUserRequest.type, handleCreateNewUser);
  yield takeLatest(getUserRequest.type, handleGetUser);
  yield takeLatest(updateUserRequest.type, handleUpdateUser);
  yield takeLatest(deleteUserRequest.type, handleDeleteUser);
}
