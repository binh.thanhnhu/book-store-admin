import { IParam, IUser } from "constants/interface";
import addParams from "utils/addParams";
import callApi from "utils/apiCaller";

export const getAllUsersByOptionAPI = (params: IParam) => {
  const url = "api/user/option?" + addParams("", params);
  return callApi("GET", url);
};

export const createNewUserAPI = (values: IUser, token: string) => {
  return callApi("POST", "api/user/register", values, token);
};

export const getUserAPI = (id: string) => {
  return callApi("GET", `api/user/get-by-id/${id}`);
};

export const updateUserAPI = (id: string, values: IUser, token: string) => {
  return callApi("PUT", `api/user/update/${id}`, values, token);
};

export const deleteUserAPI = (id: string, token: string) => {
  return callApi("DELETE", `api/user/delete/${id}`, null, token);
};
