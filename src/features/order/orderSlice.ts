import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { IOrder, IParam, IResponse } from "constants/interface";

interface IOrderState {
  loading: boolean;
  message: {
    getAllOrdersByOption: string;
    deleteOrder: string;
    createNewOrder: string;
    updateOrder: string;
    changeStatusOrder: string;
  };
  error: {
    getAllOrdersByOption: string;
    deleteOrder: string;
    createNewOrder: string;
    updateOrder: string;
    getOrder: string;
    changeStatusOrder: string;
  };
  orders: IOrder[];
  order: IOrder | null;
  totalPages: number;
  totalOrders: number;
}

const initialState: IOrderState = {
  loading: false,
  message: {
    getAllOrdersByOption: "",
    deleteOrder: "",
    createNewOrder: "",
    updateOrder: "",
    changeStatusOrder: "",
  },
  error: {
    getAllOrdersByOption: "",
    deleteOrder: "",
    createNewOrder: "",
    updateOrder: "",
    getOrder: "",
    changeStatusOrder: "",
  },
  orders: [],
  order: null,
  totalPages: 0,
  totalOrders: 0,
};

const orderSlice = createSlice({
  name: "order",
  initialState,
  reducers: {
    getAllOrdersByOptionRequest: (
      state,
      { payload }: PayloadAction<IParam>
    ) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getAllOrdersByOptionSuccess: (state, { payload }: PayloadAction<any>) => {
      state.orders = payload.category;
      state.totalPages = payload.totalPages || state.totalPages;
      state.totalOrders = payload.totalOrders || state.totalOrders;
      state.error.getAllOrdersByOption = "";
      state.loading = false;
    },
    getAllOrdersByOptionFailure: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.loading = false;
      state.error.getAllOrdersByOption = payload.message;
    },
    createNewOrderRequest: (state, { payload }: PayloadAction<IOrder>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    createNewOrderSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.error.createNewOrder = "";
      state.message.createNewOrder = payload.message;
      state.loading = false;
    },
    createNewOrderFailure: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.message.createNewOrder = "";
      state.error.createNewOrder = payload.message;
      state.loading = false;
    },
    getOrderRequest: (state, { payload }: PayloadAction<string>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getOrderSuccess: (state, { payload }: PayloadAction<IResponse<IOrder>>) => {
      state.order = payload.data;
      state.loading = false;
    },
    getOrderFailure: (state, { payload }: PayloadAction<IResponse<IOrder>>) => {
      state.order = null;
      state.error.getOrder = payload.message;
      state.loading = false;
    },
    updateOrderRequest: (state, { payload }: PayloadAction<IOrder>) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    updateOrderSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.error.updateOrder = "";
      state.message.updateOrder = payload.message;
      state.loading = false;
    },
    updateOrderFailure: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.message.updateOrder = "";
      state.error.updateOrder = payload.message;
      state.loading = false;
    },
    deleteOrderRequest: (state, { payload }: PayloadAction<string>) => {
      state.message = initialState.message;
      state.error = initialState.error;
    },
    deleteOrderSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.message.deleteOrder = payload.message;
      state.error.deleteOrder = "";
    },
    deleteOrderFailure: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.error.deleteOrder = payload.message;
      state.message.deleteOrder = "";
    },
    changeStatusOrderRequest: (
      state,
      { payload }: PayloadAction<{ id: string; status: IOrder["status"] }>
    ) => {
      state.message = initialState.message;
      state.error = initialState.error;
    },
    changeStatusOrderSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.error.changeStatusOrder = "";
      state.order = payload.data;
      state.message.changeStatusOrder = payload.message;
    },
    changeStatusOrderFailure: (
      state,
      { payload }: PayloadAction<IResponse<IOrder>>
    ) => {
      state.message.changeStatusOrder = "";
      state.error.changeStatusOrder = payload.message;
    },
  },
});

export const {
  getAllOrdersByOptionRequest,
  getAllOrdersByOptionSuccess,
  getAllOrdersByOptionFailure,
  createNewOrderRequest,
  createNewOrderSuccess,
  createNewOrderFailure,
  getOrderRequest,
  getOrderSuccess,
  getOrderFailure,
  updateOrderRequest,
  updateOrderSuccess,
  updateOrderFailure,
  deleteOrderRequest,
  deleteOrderSuccess,
  deleteOrderFailure,
  changeStatusOrderRequest,
  changeStatusOrderSuccess,
  changeStatusOrderFailure,
} = orderSlice.actions;
export default orderSlice.reducer;
