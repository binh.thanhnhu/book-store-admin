import { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { ROUTES } from "constants/data";
import { ICategory, IOrder, IParam, IResponse } from "constants/interface";
import {
  addNotificationRequest,
  startLoadingGlobal,
  stopLoadingGlobal,
} from "features/global/globalSlice";
import moment from "moment";
import { toast } from "react-toastify";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  changeStatusOrderAPI,
  createNewOrderAPI,
  deleteOrderAPI,
  getAllOrdersByOptionAPI,
  getOrderAPI,
  updateOrderAPI,
} from "./orderAPI";
import {
  changeStatusOrderFailure,
  changeStatusOrderRequest,
  changeStatusOrderSuccess,
  createNewOrderFailure,
  createNewOrderRequest,
  createNewOrderSuccess,
  deleteOrderFailure,
  deleteOrderRequest,
  deleteOrderSuccess,
  getAllOrdersByOptionFailure,
  getAllOrdersByOptionRequest,
  getAllOrdersByOptionSuccess,
  getOrderFailure,
  getOrderRequest,
  getOrderSuccess,
  updateOrderFailure,
  updateOrderRequest,
  updateOrderSuccess,
} from "./orderSlice";

function* handleGetAllOrdersByOption({ payload }: PayloadAction<IParam>) {
  try {
    const temp = { ...payload, keywordAllFieldFix: "" };
    const res: AxiosResponse<IResponse<ICategory>> = yield call(
      getAllOrdersByOptionAPI,
      temp
    );
    yield put(getAllOrdersByOptionSuccess(res.data.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getAllOrdersByOptionFailure(error.response.data));
    yield put(stopLoadingGlobal());
  }
}

function* handleCreateNewOrder({ payload }: PayloadAction<IOrder>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IOrder>> = yield call(
      createNewOrderAPI,
      payload
    );
    yield put(createNewOrderSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(createNewOrderFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleGetOrder({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IOrder>> = yield call(
      getOrderAPI,
      payload
    );
    yield put(getOrderSuccess(res.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getOrderFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleUpdateOrder({
  payload: { id, ...values },
}: PayloadAction<any>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IOrder>> = yield call(
      updateOrderAPI,
      id,
      values
    );
    yield put(updateOrderSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.order.data.update?.path}/${id}`,
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(updateOrderFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.order.data.update?.path}/${id}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleDeleteOrder({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IOrder>> = yield call(
      deleteOrderAPI,
      payload
    );
    yield put(stopLoadingGlobal());
    const { params } = yield select((state: RootState) => state.order);
    yield put(getAllOrdersByOptionRequest(params));
    yield put(deleteOrderSuccess(res.data));
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(deleteOrderFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.order.data.list?.path}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleChangeStatusOrder({
  payload,
}: PayloadAction<{ id: string; status: IOrder["status"] }>) {
  const { id, status } = payload;
  try {
    yield put(startLoadingGlobal());
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IOrder>> = yield call(
      changeStatusOrderAPI,
      id,
      status,
      token
    );
    yield put(changeStatusOrderSuccess(res.data));
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.order.data.view?.path}/${id}`,
        toast: () => toast.success(res.data.message),
      })
    );
    yield put(getOrderRequest(id));
  } catch (error) {
    yield put(changeStatusOrderFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.order.data.view?.path}/${id}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

export default function* orderSaga() {
  yield takeLatest(
    getAllOrdersByOptionRequest.type,
    handleGetAllOrdersByOption
  );
  yield takeLatest(createNewOrderRequest.type, handleCreateNewOrder);
  yield takeLatest(getOrderRequest.type, handleGetOrder);
  yield takeLatest(updateOrderRequest.type, handleUpdateOrder);
  yield takeLatest(deleteOrderRequest.type, handleDeleteOrder);
  yield takeLatest(changeStatusOrderRequest.type, handleChangeStatusOrder);
}
