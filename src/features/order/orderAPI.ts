import { IOrder, IParam } from "constants/interface";
import addParams from "utils/addParams";
import callApi from "utils/apiCaller";

export const getAllOrdersByOptionAPI = (params: IParam) => {
  const url = "api/order/option?" + addParams("", params);
  return callApi("GET", url);
};

export const createNewOrderAPI = (values: IOrder) => {
  return callApi("POST", "api/order/register", values);
};

export const getOrderAPI = (id: string) => {
  return callApi("GET", `api/order/get-by-id/${id}`);
};

export const updateOrderAPI = (id: string, values: IOrder) => {
  return callApi("PUT", `api/order/update/${id}`, values);
};

export const deleteOrderAPI = (id: string) => {
  return callApi("DELETE", `api/order/delete/${id}`);
};

export const changeStatusOrderAPI = (
  id: string,
  status: IOrder["status"],
  token: string
) => {
  return callApi(
    "GET",
    `api/order/change-status/${id}/q?status=${status}`,
    null,
    token
  );
};
