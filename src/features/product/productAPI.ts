import { IParam, IProduct } from "constants/interface";
import addParams from "utils/addParams";
import callApi from "utils/apiCaller";

export const getAllProductsAPI = (): any => callApi("GET", "api/product");

export const getAllProductsByOptionAPI = (params: IParam) => {
  const url = "api/product/option?" + addParams("", params);
  return callApi("GET", url);
};

export const createNewProductAPI = (values: IProduct, token: string) => {
  return callApi("POST", "api/product/create", values, token, "file");
};

export const getProductAPI = (id: string) => {
  return callApi("GET", `api/product/get-by-id/${id}`);
};

export const updateProductAPI = (
  id: string,
  values: IProduct & string[],
  token: string
) => {
  return callApi("PUT", `api/product/update/${id}`, values, token);
};

export const deleteProductAPI = (id: string, token: string) => {
  return callApi("DELETE", `api/product/delete/${id}`, null, token);
};

export const uploadFileAPI = (values: any, token: string) => {
  return callApi("POST", "api/product/uploadFile", values, token);
};
