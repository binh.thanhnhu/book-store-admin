import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICategory, IParam, IProduct } from "constants/interface";

interface IProductState {
  loading: boolean;
  loading2: boolean;
  products: IProduct[];
  product: IProduct | null;
  totalPages: number;
  totalProducts: number;
  error: {
    getAllProductsByOption: string;
    createNewProduct: string;
    updateProduct: string;
    getProduct: string;
    deleteProduct: string;
  };
  message: {
    getAllProductsByOption: string;
    createNewProduct: string;
    updateProduct: string;
    getProduct: string;
    deleteProduct: string;
  };
  params: IParam;
}

const initialState: IProductState = {
  loading: false,
  loading2: false,
  products: [],
  product: null,
  totalPages: 0,
  totalProducts: 0,
  error: {
    getAllProductsByOption: "",
    createNewProduct: "",
    updateProduct: "",
    getProduct: "",
    deleteProduct: "",
  },
  message: {
    getAllProductsByOption: "",
    createNewProduct: "",
    updateProduct: "",
    getProduct: "",
    deleteProduct: "",
  },
  params: {},
};

const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {
    getAllProductsRequest: (state) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getAllProductsByOptionRequest: (
      state,
      { payload }: PayloadAction<IParam>
    ) => {
      state.loading = true;
      state.params = payload;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getAllProductsByOtionSuccess: (
      state,
      { payload }: PayloadAction<ICategory>
    ) => {
      state.products = payload.category;
      state.loading = false;
      state.totalPages = payload.totalPages || state.totalPages;
      state.totalProducts = payload.totalProducts || state.totalProducts;
      state.error.getAllProductsByOption = "";
    },
    getAllProductsByOptionFailure: (
      state,
      { payload }: PayloadAction<Error>
    ) => {
      const { keywordAllField } = state.params;
      state.loading = false;
      state.error.getAllProductsByOption = payload.message;
      if (keywordAllField) {
        state.products = [];
        state.totalPages = 1;
        state.totalProducts = 0;
      }
    },
    createNewProductRequest: (state, { payload }) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    createNewProductSuccess: (state, { payload }) => {
      state.message.createNewProduct = payload.message;
      state.loading = false;
      state.error.createNewProduct = "";
    },
    createNewProductFailure: (state, { payload }) => {
      state.loading = false;
      state.error.createNewProduct = payload.message;
      state.message.createNewProduct = "";
    },
    getProductRequest: (state, { payload }) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    getProductSuccess: (state, { payload }) => {
      state.product = payload.data;
      state.loading = false;
    },
    getProductFailure: (state, { payload }) => {
      state.product = null;
      state.error.getProduct = payload.message;
      state.loading = false;
    },
    updateProductRequest: (state, { payload }) => {
      state.loading = true;
      state.message = initialState.message;
      state.error = initialState.error;
    },
    updateProductSuccess: (state, { payload }) => {
      state.message.updateProduct = payload.message;
      state.error.updateProduct = "";
      state.loading = false;
    },
    updateProductFailure: (state, { payload }) => {
      state.error.updateProduct = payload.message;
      state.message.updateProduct = "";
      state.loading = false;
    },
    deleteProductRequest: (state, { payload }) => {
      state.message = initialState.message;
      state.error = initialState.error;
    },
    deleteProductSuccess: (state, { payload }) => {
      state.message.deleteProduct = payload.message;
      state.error.deleteProduct = "";
    },
    deleteProductFailure: (state, { payload }) => {
      state.error.deleteProduct = payload.message;
      state.message.deleteProduct = "";
    },
    uploadFileRequest: (state, { payload }) => {},
  },
});

export const {
  getAllProductsByOptionRequest,
  getAllProductsByOtionSuccess,
  getAllProductsByOptionFailure,
  createNewProductRequest,
  createNewProductSuccess,
  createNewProductFailure,
  getProductRequest,
  getProductSuccess,
  getProductFailure,
  updateProductRequest,
  updateProductSuccess,
  updateProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  deleteProductFailure,
  uploadFileRequest,
} = productSlice.actions;
export default productSlice.reducer;
