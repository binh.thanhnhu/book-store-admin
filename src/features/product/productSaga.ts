import { PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { ROUTES } from "constants/data";
import { ICategory, IParam, IProduct, IResponse } from "constants/interface";
import {
  addNotificationRequest,
  startLoadingGlobal,
  stopLoadingGlobal,
} from "features/global/globalSlice";
import moment from "moment";
import { toast } from "react-toastify";
import { call, put, select, takeLatest } from "redux-saga/effects";
import {
  createNewProductAPI,
  deleteProductAPI,
  getAllProductsByOptionAPI,
  getProductAPI,
  updateProductAPI,
} from "./productAPI";
import {
  createNewProductFailure,
  createNewProductRequest,
  createNewProductSuccess,
  deleteProductFailure,
  deleteProductRequest,
  deleteProductSuccess,
  getAllProductsByOptionFailure,
  getAllProductsByOptionRequest,
  getAllProductsByOtionSuccess,
  getProductFailure,
  getProductRequest,
  getProductSuccess,
  updateProductFailure,
  updateProductRequest,
  updateProductSuccess,
} from "./productSlice";

function* handleGetAllProductsByOption({ payload }: PayloadAction<IParam>) {
  try {
    const temp = { ...payload, keywordAllFieldFix: "" };
    const res: AxiosResponse<IResponse<ICategory>> = yield call(
      getAllProductsByOptionAPI,
      temp
    );
    yield put(getAllProductsByOtionSuccess(res.data.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getAllProductsByOptionFailure(error.response.data));
    yield put(stopLoadingGlobal());
  }
}

function* handleCreateNewProduct({
  payload,
}: PayloadAction<IProduct & { imagesQuantity: number }>) {
  try {
    yield put(startLoadingGlobal());

    const { imagesQuantity, ...values } = payload;
    if (imagesQuantity > 5) {
      yield put(
        updateProductFailure({ message: "Chỉ chọn được tối đa 5 ảnh" })
      );
      yield put(stopLoadingGlobal());
      toast.error("Chỉ chọn được tối đa 5 ảnh");
      return;
    }
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IProduct>> = yield call(
      createNewProductAPI,
      values,
      token
    );
    yield put(createNewProductSuccess(res.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(createNewProductFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleGetProduct({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const res: AxiosResponse<IResponse<IProduct>> = yield call(
      getProductAPI,
      payload
    );
    yield put(getProductSuccess(res.data));
    yield put(stopLoadingGlobal());
  } catch (error) {
    yield put(getProductFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

function* handleUpdateProduct({ payload }: PayloadAction<any>) {
  const { id, image, imagesInfo, ...values } = payload;
  try {
    yield put(startLoadingGlobal());

    if (imagesInfo.quantity > 5) {
      yield put(
        updateProductFailure({ message: "Chỉ chọn được tối đa 5 ảnh" })
      );
      yield put(getProductRequest(id));
      toast.error("Chỉ chọn được tối đa 5 ảnh");
      return;
    }
    if (imagesInfo.fileSize > 2 * 1024 * 1024) {
      yield put(updateProductFailure({ message: "Chỉ upload tối đa 2MB" }));
      yield put(getProductRequest(id));
      toast.error("Chỉ upload tối đa 2MB");
      return;
    }
    if (image && image.length > 0) {
      for (let i = 0; i < image.length; i++) {
        const { fileType } = image[i].info;
        if (fileType !== "image/jpeg" && fileType !== "image/png") {
          yield put(
            updateProductFailure({ message: "Upload không đúng định dạng ảnh" })
          );
          yield put(getProductRequest(id));
          toast.error("Upload không đúng định dạng ảnh");
          return;
        }
      }
    }
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IProduct>> = yield call(
      updateProductAPI,
      id,
      { ...values, image, imagesInfo },
      token
    );
    yield put(updateProductSuccess(res.data));
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.product.data.update?.path}/${id}`,
        toast: () => toast.success(res.data.message),
      })
    );
    yield put(getProductRequest(id));
  } catch (error) {
    yield put(updateProductFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.product.data.update?.path}/${id}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
    yield put(getProductRequest(id));
  }
}

function* handleDeleteProduct({ payload }: PayloadAction<string>) {
  try {
    yield put(startLoadingGlobal());

    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IProduct>> = yield call(
      deleteProductAPI,
      payload,
      token
    );
    yield put(stopLoadingGlobal());
    const { params } = yield select((state: RootState) => state.product);
    yield put(getAllProductsByOptionRequest(params));
    yield put(deleteProductSuccess(res.data));
    yield put(
      addNotificationRequest({
        option: "success",
        message: res.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        toast: () => toast.success(res.data.message),
      })
    );
  } catch (error) {
    yield put(deleteProductFailure(error.response.data));
    yield put(stopLoadingGlobal());
    yield put(
      addNotificationRequest({
        option: "error",
        message: error.response.data.message,
        time: moment(new Date(), "MM-DD-YYYY HH:mm:ss").format(),
        path: `${ROUTES.product.data.list?.path}`,
        toast: () => toast.error(error.response.data.message),
      })
    );
  }
}

export default function* productSaga() {
  yield takeLatest(
    getAllProductsByOptionRequest.type,
    handleGetAllProductsByOption
  );
  yield takeLatest(createNewProductRequest.type, handleCreateNewProduct);
  yield takeLatest(getProductRequest.type, handleGetProduct);
  yield takeLatest(updateProductRequest.type, handleUpdateProduct);
  yield takeLatest(deleteProductRequest.type, handleDeleteProduct);
}
