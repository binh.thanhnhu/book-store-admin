import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ROUTES } from "constants/data";
import {
  IMessage,
  INotification,
  IResponse,
  keyRoutes,
} from "constants/interface";

interface IGlobalState {
  loading: boolean;
  backgroundLoading: boolean;
  darkmode: boolean;
  notifications: INotification[];
  messages: IMessage[];
  error: {
    messages: string;
  };
}

const initialState: IGlobalState = {
  loading: false,
  backgroundLoading: true,
  darkmode: JSON.parse(localStorage.getItem("darkmode") || "false"),
  notifications: JSON.parse(localStorage.getItem("notification") || "[]"),
  messages: JSON.parse(localStorage.getItem("message") || "[]"),
  error: {
    messages: "",
  },
};

const globalSlice = createSlice({
  name: "global",
  initialState,
  reducers: {
    startLoadingGlobal: (state) => {
      state.loading = true;
    },
    stopLoadingGlobal: (state) => {
      state.loading = false;
    },
    startLoadingBackground: (state) => {
      state.backgroundLoading = true;
    },
    stopLoadingBackground: (state) => {
      state.backgroundLoading = false;
    },
    changeDarkMode: (state, { payload }: PayloadAction<boolean>) => {
      state.darkmode = payload;
      localStorage.setItem("darkmode", JSON.stringify(payload));
    },
    addNotificationRequest: (
      state,
      {
        payload,
      }: PayloadAction<{
        option: INotification["option"];
        message: string;
        time: string;
        path?: string;
        toast?: () => void;
      }>
    ) => {
      payload.toast && payload.toast();
      state.notifications.unshift({
        option: payload.option,
        message: payload.message,
        time: payload.time,
        path: payload.path,
        seen: false,
      });
      localStorage.setItem("notification", JSON.stringify(state.notifications));
    },
    updateNotificationRequest: (state, { payload }: PayloadAction<number>) => {
      const length =
        state.notifications.length > payload
          ? payload
          : state.notifications.length;
      if (state.notifications.length > 0) {
        for (let i = 0; i < length; i++) {
          state.notifications[i].seen = true;
        }
      }
      localStorage.setItem("notification", JSON.stringify(state.notifications));
    },
    updateMessageRequest: (state, { payload }: PayloadAction<number>) => {
      const length =
        state.messages.length > payload ? payload : state.messages.length;
      if (state.messages.length > 0) {
        for (let i = 0; i < length; i++) {
          state.messages[i].seen = true;
        }
      }
      localStorage.setItem("message", JSON.stringify(state.messages));
    },
    clearNotificationRequest: (state) => {
      state.notifications = [];
      localStorage.setItem("notification", JSON.stringify("[]"));
    },
    clearMessageRequest: (state) => {
      state.messages = state.messages.map((e) => {
        e.seen = true;
        return e;
      });
      localStorage.setItem("message", JSON.stringify(state.messages));
    },
    getAllMessagesRequest: () => {},
    refreshAllMessagesRequest: () => {},
    getAllMessagesSuccess: (
      state,
      { payload }: PayloadAction<IResponse<IMessage[]>>
    ) => {
      const temp: IMessage[] = payload.data.filter(
        ({ _id: id1 }) => !state.messages.some(({ _id: id2 }) => id2 === id1)
      );
      const values = temp.map((t) => {
        t.path = t.route
          ? `${ROUTES[t.route as keyRoutes].data.update?.path}/${
              t.suffix || ""
            }`
          : undefined;
        return t;
      });
      state.messages.unshift(...values.reverse());
      localStorage.setItem("message", JSON.stringify(state.messages));
    },
    getAllMessagesFailure: (
      state,
      { payload }: PayloadAction<IResponse<IMessage>>
    ) => {
      state.error.messages = payload.message;
    },
  },
});

export const {
  startLoadingGlobal,
  stopLoadingGlobal,
  startLoadingBackground,
  stopLoadingBackground,
  changeDarkMode,
  addNotificationRequest,
  updateNotificationRequest,
  updateMessageRequest,
  clearNotificationRequest,
  clearMessageRequest,
  getAllMessagesRequest,
  refreshAllMessagesRequest,
  getAllMessagesSuccess,
  getAllMessagesFailure,
} = globalSlice.actions;
export default globalSlice.reducer;
