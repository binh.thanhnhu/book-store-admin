import callApi from "utils/apiCaller";

export const getAllMessagesAPI = (token: string) => {
  return callApi("GET", "api/message/getAll", null, token);
};
