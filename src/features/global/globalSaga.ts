import { RootState } from "app/store";
import { AxiosResponse } from "axios";
import { IMessage, IResponse } from "constants/interface";
import {
  getAllMessagesFailure,
  getAllMessagesRequest,
  getAllMessagesSuccess,
  refreshAllMessagesRequest,
  stopLoadingBackground,
} from "features/global/globalSlice";
import { call, put, select, takeLatest } from "redux-saga/effects";
import { getAllMessagesAPI } from "./globalAPI";

function* handleGetAllMessages() {
  try {
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IMessage[]>> = yield call(
      getAllMessagesAPI,
      token
    );
    yield put(getAllMessagesSuccess(res.data));
  } catch (error) {
    yield put(getAllMessagesFailure(error.response.data));
  }
}

function* handleRefreshAllMessages() {
  try {
    const { token } = yield select((state: RootState) => state.auth);
    const res: AxiosResponse<IResponse<IMessage[]>> = yield call(
      getAllMessagesAPI,
      token
    );
    yield put(getAllMessagesSuccess(res.data));
    yield put(stopLoadingBackground());
  } catch (error) {
    yield put(getAllMessagesFailure(error.response.data));
    yield put(stopLoadingBackground());
  }
}

export default function* userSaga() {
  yield takeLatest(getAllMessagesRequest.type, handleGetAllMessages);
  yield takeLatest(refreshAllMessagesRequest.type, handleRefreshAllMessages);
}
