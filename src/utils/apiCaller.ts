import axios, { AxiosResponse, Method } from "axios";
const API_URL = process.env.REACT_APP_API_URL || "http://localhost:5000";

export default function callApi(
  method: Method,
  endpoint: string,
  data?: any,
  token?: string,
  file?: "file"
): Promise<AxiosResponse> {
  if (token) {
    axios.defaults.headers.common = { Authorization: `Bearer ${token}` };
  }
  if (file) {
    axios.defaults.headers.post["Content-Type"] = "multipart/form-data";
  }
  return axios({
    method,
    url: `${API_URL}/${endpoint}`,
    data,
  });
}
