export default function calcItems(
  totalItems: number,
  perPage: string,
  page: string,
  type: "asc" | "desc"
) {
  let min = 1;
  let max = 1;

  const _page = parseInt(page || "1");
  const _perPage = parseInt(perPage || "12");

  if (type === "desc") {
    max =
      Math.ceil(totalItems / _perPage) === _page
        ? totalItems % _perPage || _perPage
        : (Math.ceil(totalItems / _perPage) - _page) * _perPage +
          (totalItems % _perPage || _perPage);
    min = max - _perPage;
  } else {
    max =
      Math.ceil(totalItems / _perPage) === _page
        ? _perPage * (_page - 1) + (totalItems % _perPage || _perPage)
        : _perPage * _page;
    min = (_page - 1) * _perPage + 1;
  }

  return { min, max };
}
