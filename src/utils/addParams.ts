import { IParam } from "constants/interface";

export default function addParams(
  searchUrl: string,
  rest?: IParam,
  except?: string[] | "all"
): string {
  const urlParams = new URLSearchParams(searchUrl);
  let query: any = {};
  let url = "";

  urlParams.forEach((value, key) => {
    if (!except?.includes(key)) query[key] = value;
  });
  query = except === "all" ? rest : { ...query, ...rest };
  Object.keys(query).map((key) => {
    if (query[key]) url += `${key}=${query[key]}&`;
    return null;
  });

  return url;
}
