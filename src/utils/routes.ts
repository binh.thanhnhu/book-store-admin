import { IRoutes } from "constants/interface";

// recursive function for find route name
const loop: any = (pathname: string, object: any, arr: string[] = []) => {
  let i = 0;
  let redundantKey: any;
  for (let key in object) {
    redundantKey = key;
    if (typeof object[key] === "object") {
      i++;
      if (i > 1) arr.pop();
      arr.push(key);
    }
    if (object[key].path === pathname) {
      return { route: object[key], routesArray: arr };
    }
    if (typeof object[key] === "object") {
      const { route, routesArray } = loop(pathname, object[key], arr);
      if (route && routesArray) return { route, routesArray };
    }
  }
  if (typeof object[redundantKey] === "object") arr.pop();
  return { route: null, routesArray: null };
};

// find route name tree for breadcrumb
const findRouteName = (location: any, routes: IRoutes) => {
  let arr: number[] = [];
  for (let index: number = 0; index < location.pathname.length; index++) {
    if (location.pathname[index] === "/") {
      arr.push(index);
    }
  }
  const pathname = location.pathname.substring(0, arr[2]);
  const { routesArray } = loop(pathname, routes);
  return { routesArray };
};

export { findRouteName };
